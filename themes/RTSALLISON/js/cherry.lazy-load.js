function getWindowHeight() {
    var myWidth = 0, myHeight = 0;
    if( typeof( window.innerWidth ) == 'number' ) {
        //Non-IE
        myHeight = window.innerHeight;
    } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
        //IE 6+ in 'standards compliant mode'
        myHeight = document.documentElement.clientHeight;
    } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
        //IE 4 compatible
        myHeight = document.body.clientHeight;
    }

    return myHeight
}

function appearBox(element, element_top, bottom_of_window) {
    /* If the object is completely visible in the window, fade it it */
    var buffer = element.outerHeight()/2;
    if( bottom_of_window > element_top + buffer) {
        setTimeout(function(){
            element.removeClass('trigger').animate({'opacity':'1'}, element.data('speed'))
        }, element.data('delay'));            
    }
}


(function($) {
    $(window).load(function() {
        if(!device.mobile() && !device.tablet()){
            $('.lazy-load-box').each( function(i){
                var element_offset = $(this).offset(),
                    element_top = element_offset.top;
                    bottom_of_window = $(window).scrollTop() + getWindowHeight();
                
                appearBox($(this), element_top, bottom_of_window);
            });

            /* Every time the window is scrolled ... */
            $(window).scroll( function() {
                /* Check the location of each desired element */
                $('.lazy-load-box').each( function(i){
                    
                    var element_offset = $(this).offset(),
                        element_top = element_offset.top;
                        bottom_of_window = $(window).scrollTop() + getWindowHeight();
                    
                    appearBox($(this), element_top, bottom_of_window);
                    
                }); 
            
            });
        } else {
            $('.lazy-load-box').each( function(i) {
                $(this).removeClass('trigger').css('opacity', '1');
            });
        }
    });
})(jQuery);
/*
     FILE ARCHIVED ON 14:16:34 Feb 19, 2019 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 05:08:49 Jan 21, 2020.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  exclusion.robots.policy: 0.177
  PetaboxLoader3.resolve: 326.349 (2)
  esindex: 0.025
  PetaboxLoader3.datanode: 135.198 (5)
  exclusion.robots: 0.191
  load_resource: 512.605
  RedisCDXSource: 2.697
  CDXLines.iter: 15.296 (3)
  captures_list: 65.037
  LoadShardBlock: 42.939 (3)
*/