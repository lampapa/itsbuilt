(function(){
	/**
	 * If we have a temp variable of type function it means lodash was loaded before underscore so we need to
	 * remove the reference to underscore from window._ by using the method .noConflict() from underscore, after
	 * this point we need to revert back the value of window._ which was lodash.
	 *
	 * In the second scenario when underscore is loaded before lodash, this will not be executed as window._ will remain as lodash.
	 *
	 * On a third scenario when lodash is not included this will either be executed which will allow to use something like: window.underscore || window._ to fallback to the correct value of underscore in the plugins.
	 */
	if ( window._lodash_tmp !== false && typeof window._lodash_tmp === 'function' ) {
		// Remove reference to _ if is underscore
		window.underscore = _.noConflict();
		// Restore reference to lodash if present
		window._ = window._lodash_tmp;
	}
})();

/*
     FILE ARCHIVED ON 14:16:40 Feb 19, 2019 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 09:05:01 Jan 16, 2020.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  CDXLines.iter: 20.259 (3)
  PetaboxLoader3.datanode: 74.396 (4)
  exclusion.robots: 0.344
  esindex: 0.024
  exclusion.robots.policy: 0.322
  load_resource: 1097.712
  LoadShardBlock: 62.615 (3)
  captures_list: 94.783
  PetaboxLoader3.resolve: 1061.336
  RedisCDXSource: 6.428
*/