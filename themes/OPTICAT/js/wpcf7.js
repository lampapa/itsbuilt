jQuery(document).ready(function($) {
// clear cf7 error msg on mouseover
$(".wpcf7-form-control-wrap").mouseover(function(){
	$obj = $("span.wpcf7-not-valid-tip",this);
		$obj.fadeOut();
	});
});

/*
     FILE ARCHIVED ON 21:59:11 Feb 10, 2019 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 09:05:55 Jan 16, 2020.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  exclusion.robots.policy: 0.216
  CDXLines.iter: 12.874 (3)
  LoadShardBlock: 112.828 (3)
  captures_list: 135.583
  RedisCDXSource: 6.144
  esindex: 0.013
  load_resource: 213.554
  PetaboxLoader3.datanode: 146.47 (5)
  exclusion.robots: 0.233
  PetaboxLoader3.resolve: 103.294 (2)
*/