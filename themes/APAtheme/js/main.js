(function($) {
  "use strict";




  $(window).on('scroll', function() {
    var scroll = $(window).scrollTop();
    if (scroll < 100) {
      $(".sticky-header").removeClass("sticky");
    } else {
      $(".sticky-header").addClass("sticky");
    }
  });
  $.fn.handleCounter = function (options) {
      var $input,
          $btnMinus,
          $btnPlugs,
          minimum,
          maximize,
          writable,
          onChange,
          onMinimum,
          onMaximize;
      var $handleCounter = this
      $btnMinus = $handleCounter.find('.counter-minus')
      $input = $handleCounter.find('input')
      $btnPlugs = $handleCounter.find('.counter-plus')
      var defaultOpts = {
          writable: true,
          minimum: 1,
          maximize: null,
          onChange: function(){},
          onMinimum: function(){},
          onMaximize: function(){}
      }
      var settings = $.extend({}, defaultOpts, options)
      minimum = settings.minimum
      maximize = settings.maximize
      writable = settings.writable
      onChange = settings.onChange
      onMinimum = settings.onMinimum
      onMaximize = settings.onMaximize
      if (!$.isNumeric(minimum)) {
          minimum = defaultOpts.minimum
      }
      if (!$.isNumeric(maximize)) {
          maximize = defaultOpts.maximize
      }
      var inputVal = $input.val()
      if (isNaN(parseInt(inputVal))) {
          inputVal = $input.val(0).val()
      }
      if (!writable) {
          $input.prop('disabled', true)
      }

      changeVal(inputVal)
      $input.val(inputVal)
      $btnMinus.click(function () {
          var num = parseInt($input.val())
          if (num > minimum) {
              $input.val(num - 1)
              changeVal(num - 1)
          }
      })
      $btnPlugs.click(function () {
          var num = parseInt($input.val())
          if (maximize==null||num < maximize) {
              $input.val(num + 1)
              changeVal(num + 1)
          }
      })
      var keyUpTime
      $input.keyup(function () {
          clearTimeout(keyUpTime)
          keyUpTime = setTimeout(function() {
              var num = $input.val()
              if (num == ''){
                  num = minimum
                  $input.val(minimum)
              }
              var reg = new RegExp("^[\\d]*$")
              if (isNaN(parseInt(num)) || !reg.test(num)) {
                  $input.val($input.data('num'))
                  changeVal($input.data('num'))
              } else if (num < minimum) {
                  $input.val(minimum)
                  changeVal(minimum)
              }else if (maximize!=null&&num > maximize) {
                  $input.val(maximize)
                  changeVal(maximize)
              } else {
                  changeVal(num)
              }
          },300)
      })
      $input.focus(function () {
          var num = $input.val()
          if (num == 0) $input.select()
      })

      function changeVal(num) {
          $input.data('num', num)
          $btnMinus.prop('disabled', false)
          $btnPlugs.prop('disabled', false)
          if (num <= minimum) {
              $btnMinus.prop('disabled', true)
              onMinimum.call(this, num)
          } else if (maximize!=null&&num >= maximize) {
              $btnPlugs.prop('disabled', true)
              onMaximize.call(this, num)
          }
          onChange.call(this, num)
      }
      return $handleCounter
  };

    /*Video-Slide*/
    $(document).ready(function() {
        $('.video_slide').carousel({
            num: 7,
            maxWidth: 450,
            maxHeight: 300,
            distance: 50,
            scale: 0.6,
            animationTime: 700,
            showTime: 4000,

        });
});

      $(document).ready(function() {

        setTimeout(function() {
          $(".fancybox").on("click", function() {
            $.fancybox({
              'titleShow'     : true,
              'href' : this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
              'type'      : 'swf',
              'swf'       : {'wmode':'transparent','allowfullscreen':'true'}
              // href: this.href,
              // type: $(this).data("type")
            }); // fancybox
            return false
          });
        }, 1000);
      });

      $(document).on('click', '.quantity .plus, .quantity .minus', function(e) {
        // Get values
        var $qty = $(this).closest('.quantity').find('.qty'),
          currentVal = parseFloat($qty.val()),
          max = parseFloat($qty.attr('max')),
          min = parseFloat($qty.attr('min')),
          step = $qty.attr('step');
        // Format values
        if (!currentVal || currentVal === '' || currentVal === 'NaN') currentVal = 0;
        if (max === '' || max === 'NaN') max = '';
        if (min === '' || min === 'NaN') min = 0;
        if (step === 'any' || step === '' || step === undefined || parseFloat(step) === 'NaN') step = 1;
        // Change the value
        if ($(this).is('.plus')) {
          if (max && (max == currentVal || currentVal > max)) {
            $qty.val(max);
          } else {
            $qty.val(currentVal + parseFloat(step));
          }
        } else {
          if (min && (min == currentVal || currentVal < min)) {
            $qty.val(min);
          } else if (currentVal > 0) {
            $qty.val(currentVal - parseFloat(step));
          }
        }
        // Trigger change event
        $qty.trigger('change');
        e.preventDefault();
      });

      $(document).ready(function(){

      var owl_1 = $('#owl_1');
      var owl_2 = $('#owl_2');

      owl_1.owlCarousel({
        autoplay:false,
        autoplayTimeout:6000,
        loop:true,
        margin:10,
        nav:false,
        items: 1,
        dots: false
      });

      owl_2.owlCarousel({
        // autoplay:true,
        // autoplayTimeout:10000,
        // loop:true,
        margin:10,
        nav: true,
        items: 5,
        dots: false
      });

      owl_2.find(".item").click(function(){
        var slide_index = owl_2.find(".item").index(this);
        owl_1.trigger('to.owl.carousel',[slide_index,300]);
      });

      // Custom Button
      $('.next_btn').click(function() {
        owl_2.trigger('next.owl.carousel',500);
      });
      $('.prev_btn').click(function() {
        owl_2.trigger('prev.owl.carousel',500);
      });
      });

})(jQuery);
