/**
 * @file
 * @fileGlobal jQuery, Drupal */

(function ($, Drupal) {
  'use strict';
  var url = window.location.pathname.split('/');
  var path='/'+url[1]+'/admin/commerce-smart-upload';
  console.log(path);
   $('div#dropzone').dropzone({
    url: path,
    dictDefaultMessage: Drupal.t('Drag your images here or click on the box to upload')
  });

})(jQuery, Drupal);
