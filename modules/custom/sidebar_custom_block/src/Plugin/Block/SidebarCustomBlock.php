<?php

/**
 * @file
 * Contains \Drupal\my_custom_block\Plugin\Block
 */

namespace Drupal\sidebar_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
/**
 * below section is important
 * 
 * @Block(
 *  id = "sidebar_custom_block",
 *  admin_label = @Translation("Sidebar custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class SidebarCustomBlock extends BlockBase{
  public function __construct(){
    $this->connection = \Drupal::database();    
  }
 
   public function build(){  

 $months = array(   
            '01' => 'January',
            '02' => 'February',
            '03' => 'March',
            '04' => 'April',
            '05' => 'May',
            '06' => 'June',
            '07' => 'July',
            '08' => 'August',
            '09' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December');

   	$service_array = [];  
    $service_array1=[];
    $service_array2=[];

    $query = $this->connection->query(" select * from(select nid,title,entity_id,field_sequence_news_value from (select nid,title from node_field_data where type='homepage_news' and status=1 ) a join(SELECT entity_id,field_sequence_news_value FROM node__field_sequence_news) b on entity_id=nid ) c order by field_sequence_news_value DESC limit 5");
    while ($row = $query->fetchAssoc()) {
            $node_id=$row['nid'];            
            $title= $row['title'];
            $service_array[] = array('node_id'=>$node_id,'title'=>$title);
    }

     $query1 = $this->connection->query("select date_det from( select DATE_FORMAT(field_news_date_value,'%Y-%m') date_det FROM node__field_news_date order by field_news_date_value desc) a group by date_det desc");
    while ($row1 = $query1->fetchAssoc()) {
             $month_num= explode('-', $row1['date_det']);
             $month_name= $months[$month_num[1]];    
             $title=$month_name.' '.$month_num[0];  
             $node_det="year=".$month_num[0].'&month='.$month_num[1];           
             $service_array1[] = array('title'=>$title,'node_det'=>trim($node_det));
    }



    $query2 = $this->connection->query("SELECT entity_id,field_category_name_data_value FROM node_revision__field_category_name_data order by field_category_name_data_value asc");
        while ($row2 = $query2->fetchAssoc()) {
            $node_id=$row2['entity_id'];            
            $title=$row2['field_category_name_data_value'];
            $service_array2[] = array('node_id'=>$node_id,'title'=>$title);
    }



   
    /*end of sort by sequence*/

 /*   echo "<pre>";
  print_r($testimonial_array);
  print_r($service_array1);
  print_r($service_array2);
  die;*/
      return [
      '#theme' => 'sidebartemplate',
      '#test_var' => '',
      '#service_array'=>$service_array,
      '#service_array1'=>$service_array1,
      '#service_array2'=>$service_array2
    ];
  }



}

?>