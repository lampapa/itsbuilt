<?php

/**
 * @file
 * Contains \Drupal\my_custom_block\Plugin\Block
 */

namespace Drupal\apa_banner_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
/**
 * below section is important
 * 
 * @Block(
 *  id = "apabanner_custom_block",
 *  admin_label = @Translation("Apa Banner custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class ApaBannerCustomBlock extends BlockBase{
 
   public function build(){
   	$service_array = $service_array1 = $service_array2 = [];
    $nids          = \Drupal::entityQuery('node')->condition('type','homepage_block_one')->execute();
    foreach($nids as $key => $ids){               
      $node = \Drupal\node\Entity\Node::load($ids);            
      $res = $node->field_homepage_block1_sequence->getValue();
      $service_array1[] = array(
      'title'=>$node->getTitle(),
      'raw_html'=>$node->body->value,      
      'rm_url'=>$node->field_homepage_block1_readmore->value,         
      'sequence'=>$res[0]['value']
      );
    }
    
    $nids          = \Drupal::entityQuery('node')->condition('type','homepage_block_two')->execute();
    foreach($nids as $key => $ids){               
      $node = \Drupal\node\Entity\Node::load($ids);
      $res = $node->field_homepage_block2_sequence->getValue();
      $service_array2[] = array(
      'title'=>$node->getTitle(),
      'raw_html'=>$node->body->value,   
       'rm_url'=>$node->field_homepage_block2_readmore->value,          
      'sequence'=>$res[0]['value']
      );
    }

    $nids          = \Drupal::entityQuery('node')->condition('type','homepage_block_three')->execute();
    foreach($nids as $key => $ids){               
      $node = \Drupal\node\Entity\Node::load($ids);      
      $res = $node->field_homepage_block3_sequence->getValue();
      $service_array3[] = array(
      'title'=>$node->getTitle(),
      'raw_html'=>$node->body->value,   
    //  'rm_url'=>$node->field_homepage_block3_readmore->value,          
      'sequence'=>$res[0]['value']
      );
    }

    /*sort by sequence*/
    usort($service_array1, function($a, $b) {
          return $a['sequence'] <=> $b['sequence'];
    });
    usort($service_array1, function($a, $b) {
          return $a['sequence'] <=> $b['sequence'];
    });
    usort($service_array1, function($a, $b) {
          return $a['sequence'] <=> $b['sequence'];
    });
    /*end of sort by sequence*/
    /*echo "<pre>";
    print_r($service_array1);
    print_r($service_array2);
    print_r($service_array3);*/
    return [
      '#theme' => 'apabannertemplate',          
      '#service_array1'=>$service_array1,
      '#service_array2'=>$service_array2,
      '#service_array3'=>$service_array3
    ];
  }
}

?>