<?php

/**
 * @file
 * Contains \Drupal\information_custom_block\Plugin\Block
 */

namespace Drupal\information_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "information_custom_block",
 *  admin_label = @Translation("Information Custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class InformationBlock extends BlockBase{
  public function build(){
    /*  return [
      '#theme' => 'abouttemplate',
      '#test_var' => $this->t('Test Value'),
    ]; */
    $service_array ='';
    $nids = \Drupal::entityQuery('node')->condition('type','information')->execute();
    foreach ($nids as $key => $ids) {
      $node = \Drupal\node\Entity\Node::load($ids);   
      $service_array =$node->body->value;
    } 
    return array(
    '#theme' => 'informationtemplate',
    '#test_var' => $this->t('Test Value'),
    '#items'=>$service_array
    //'#title'=>'Our Article List'
    );
  }
   
}

?>