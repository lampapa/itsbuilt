<?php

/**
 * @file
 * Contains \Drupal\makemodelyear_partsearch_block\Plugin\Block
 */

namespace Drupal\makemodelyear_partsearch_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "makemodelyear_partsearch_block",
 *  admin_label = @Translation("Makemodelyear Partsearch Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class MakemodelyearBlock extends BlockBase{
 
   public function build(){
   	$seo_array=[];
   	$connection = \Drupal::database();
    $query = $connection->query("SELECT h1_tag,h2_tag FROM catapult_seo where page_name='Part Search'");
    while ($row = $query->fetchAssoc()){      
       $seo_array[] = array($row['h1_tag'],$row['h2_tag']);
    }
      return [
      '#theme' => 'makemodelyeartemplate',
      '#test_var' => $this->t('Test Value'),
      '#seo_array' => $seo_array
    ];
  }
}

?>