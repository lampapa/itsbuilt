<?php

/**
 * @file
 * Contains \Drupal\mycartwish_custom_block\Plugin\Block
 */

namespace Drupal\mycartwish_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "mycartwish_custom_block",
 *  admin_label = @Translation("Mycartwish custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class MycartwishCustomBlock extends BlockBase{
 
   public function build(){
      return [
      '#theme' => 'mycartwishtemplate',
      '#test_var' => $this->t('Test Value'),
    ];
  }
}

?>