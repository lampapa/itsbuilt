<?php
/**
 * @file
 * Contains \Drupal\amazing_forms\Form\ContributeForm.
 */

namespace Drupal\amazing_forms\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Contribute form.
 */
class ContactUsForm extends FormBase {
  /**
   * {@inheritdoc}
   */
 public function getFormId() {
    return 'amazing_forms_contactus_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $connection = \Drupal::database();
     // $makequery = $connection->query("SELECT * FROM catapult_contact_address where deletion_status='N'");
      // $arrmake = array();
      // $arrmake[''] ='Select Make';
      // while ($rec = $makequery->fetchAssoc()) {
      //  $arrmake[$rec['make']] = $rec['make'];
    
      // }  
    
    $form['company_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Company Name'),
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['company_name'])
    );
    $form['address1'] = array(
      '#type' => 'textfield',
      '#title' => t('Address1'),
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['address1'])
    );
   $form['address2'] = array(
      '#type' => 'textfield',
      '#title' => t('Address2'),
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['address2'])
    );
    $form['zip_code'] = array(
      '#type' => 'tel',
      '#title' => t('Zip Code'),
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['zip_code'])
    );
   $form['country'] = array(
      '#type' => 'textfield',
      '#title' => t('Country'),
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['country'])
    );
    $form['state'] = array(
      '#type' => 'textfield',
      '#title' => t('State'),
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['state'])
    );
   
   $form['city'] = array(
      '#type' => 'textfield',
      '#title' => t('City'),
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['city'])
    );
    $form['email'] = array(
      '#type' => 'textfield',
      '#title' => t('Email'),
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['email'])
    );
   
   $form['phone'] = array(
      '#type' => 'tel',
      '#title' => t('phone'),
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['phone'])
    );
   
    $form['email_drop_line'] = array(
      '#type' => 'textfield',
      '#title' => t('Email Id for Drop Us a line'),
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['email_drop_line'])
    );

   $form['show_map'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show Map'),
      '#attributes' => array('ng-model'=>['show_map'])
    );
   $form['google_map'] = array(
      '#type' => 'textarea',
      '#title' => t('Google Map Iframe'),
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['google_map'])
    );


    $form['savebutton'] = array(
      '#type' => 'button',
      '#attributes' => array('ng-click' => 'saveyeardata()','id'=>['myAnchor'],'class' => ['button-normal btn-save-icon']),
      '#value' => t('Save'),
    );
    
    $form['options']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
      '#submit' => array(''),
      '#attributes' => array('class' => ['button-normal btn-reset-icon'])
    );
    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function MY_MODULE_FORM_ID_reset($form, &$form_state) {
    $form_state['rebuild'] = FALSE;
  }
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // if (!$form_state->getValue('Make') || empty($form_state->getValue('Make'))) {
    //     $form_state->setErrorByName('Make', $this->t('Please Enter Make'));
    // }
    // // Assert the lastname is valid
    // if (!$form_state->getValue('Model') || empty($form_state->getValue('Model'))) {
    //     $form_state->setErrorByName('Model', $this->t('Please Enter Model'));
    // }
    
    // // Assert the subject is valid
    // if (!$form_state->getValue('FromYear') || empty($form_state->getValue('FromYear'))) {
    //     $form_state->setErrorByName('FromYear', $this->t('Please Enter From Year'));
    // }
    // // Assert the message is valid
    // if (!$form_state->getValue('ToYear') || empty($form_state->getValue('ToYear'))) {
    //     $form_state->setErrorByName('ToYear', $this->t('Plesae Enter To Year'));
    // }
   
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {


     $connection = \Drupal::database();
    $make = $model=$from_year=$to_year=$partno=$notes ='';
    foreach ($form_state->getValues() as $key => $value) {
      //echo $key ."--textvalue--".$value;
      if($key == 'Make'){
        $make =$value;
      }
      if($key == 'Model'){
        $model =$value;
      }
      if($key == 'FromYear'){
        $from_year =$value;
      }
      if($key == 'ToYear'){
        $to_year =$value;
      }
      
    }
    if($from_year < $to_year){
      for($i=$from_year;$i<=$to_year;$i++){
        $connection->query("insert into catapult_year_data(make,model,from_year,to_year,created_by,created_on,deletion_status,actual_year) Values('".$make."','".$model."','".$from_year."','".$to_year."','JP',Now(),'N','".$i."')");
      }
    }
    
  }
}