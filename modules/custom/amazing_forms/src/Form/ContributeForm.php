<?php
/**
 * @file
 * Contains \Drupal\amazing_forms\Form\ContributeForm.
 */

namespace Drupal\amazing_forms\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Contribute form.
 */
class ContributeForm extends FormBase {
  /**
   * {@inheritdoc}
   */
 public function getFormId() {
    return 'amazing_forms_contribute_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['Make'] = array(
      '#type' => 'textfield',
      '#title' => t('Make'),
    );
    $form['Model'] = array(
      '#type' => 'textfield',
      '#title' => t('Model'),
    );
    $form['FromYear'] = array(
      '#type' => 'textfield',
      '#title' => t('From Year'),
    );
    $form['ToYear'] = array(
      '#type' => 'textfield',
      '#title' => t('To Year'),
    );
    $form['PartNumber'] = array(
      '#type' => 'textarea',
      '#title' => t('Part Number'),
    );
     $form['Notes'] = array(
      '#type' => 'textarea',
      '#title' => t('Notes'),
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );
    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getValue('Make') || empty($form_state->getValue('Make'))) {
        $form_state->setErrorByName('Make', $this->t('Please Enter Make'));
    }
    // Assert the lastname is valid
    if (!$form_state->getValue('Model') || empty($form_state->getValue('Model'))) {
        $form_state->setErrorByName('Model', $this->t('Please Enter Model'));
    }
    // Assert the email is valid
    // if (!$form_state->getValue('email') || !filter_var($form_state->getValue('email'), FILTER_VALIDATE_EMAIL)) {
    //     $form_state->setErrorByName('email', $this->t('Votre adresse e-mail semble invalide.'));
    // }
    // Assert the subject is valid
    if (!$form_state->getValue('FromYear') || empty($form_state->getValue('FromYear'))) {
        $form_state->setErrorByName('FromYear', $this->t('Please Enter From Year'));
    }
    // Assert the message is valid
    if (!$form_state->getValue('ToYear') || empty($form_state->getValue('ToYear'))) {
        $form_state->setErrorByName('ToYear', $this->t('Plesae Enter To Year'));
    }
    if (!$form_state->getValue('PartNumber') || empty($form_state->getValue('PartNumber'))) {
        $form_state->setErrorByName('PartNumber', $this->t('Plesae Enter PartNo'));
    }
    if (!$form_state->getValue('Notes') || empty($form_state->getValue('Notes'))) {
        $form_state->setErrorByName('Notes', $this->t('Plesae Enter Notes'));
    } 
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {


     $connection = \Drupal::database();
    $make = $model=$from_year=$to_year=$partno=$notes ='';
    foreach ($form_state->getValues() as $key => $value) {
      //echo $key ."--textvalue--".$value;
      if($key == 'Make'){
        $make =$value;
      }
      if($key == 'Model'){
        $model =$value;
      }
      if($key == 'FromYear'){
        $from_year =$value;
      }
      if($key == 'ToYear'){
        $to_year =$value;
      }
      if($key == 'PartNumber'){
        $partno =$value;
      }
      if($key == 'Notes'){
        $notes =$value;
      }
    }
    if($from_year < $to_year){
      for($i=$from_year;$i<$to_year;$i++){
        $query = $connection->query("insert into catapult_year_data(make,model,from_year,to_year,created_by,created_on,deletion_status,partno,notes,actual_year) Values('".$make."','".$model."','".$from_year."','".$to_year."','JP',Now(),'N','".$partno."','".$notes."','".$i."')");
      }
      drupal_set_message($this->t('Thank you very much @firstname @lastname for your message. You will receive a confirmation email shortly.', [
        '@Make' => $form_state->getValue('Make'),
        '@Model' => $form_state->getValue('Model'),
      ]));
    }
    
  }
}