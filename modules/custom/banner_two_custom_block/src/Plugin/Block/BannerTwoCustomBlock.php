<?php

/**
 * @file
 * Contains \Drupal\my_custom_block\Plugin\Block
 */

namespace Drupal\banner_two_custom_block\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;


/**
 * below section is important
 * 
 * @Block(
 *  id = "banner_two_custom_block",
 *  admin_label = @Translation("Banner two custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class BannerTwoCustomBlock extends BlockBase{

 public function build(){
    $service_array =[];
    $service_array1 =[];
  $nids          = \Drupal::entityQuery('node')->condition('type','homepage_banner')->execute();
      foreach($nids as $key => $ids){
      $node = \Drupal\node\Entity\Node::load($ids);     
      $service_array[] = array(
                      'title'=>$node->getTitle(),
                      'body'=>$node->body->value,                     
                      'id'=>$ids,
                      'file'=>file_create_url($node->field_homepage_banner_upload->entity->getFileUri()),
                      'altvalue'=>$node->field_homepage_banner_upload->alt
                      );
    }
      $nids          = \Drupal::entityQuery('node')->condition('type','homepage_video')->execute();
      foreach($nids as $key => $ids){
      $node = \Drupal\node\Entity\Node::load($ids);     
      $service_array1[] = array(
                      'title'=>$node->getTitle(),
                      'body'=>$node->body->value,                     
                      'id'=>$ids
                      
                      );
    }
   // print_r($service_array);die;
      return [
      '#theme' => 'bannertwotemplate',
      '#test_var' => $this->t('Test Value'),
      '#service_array'=>$service_array,
      '#service_array1'=>$service_array1
    ];
  }
}
?>