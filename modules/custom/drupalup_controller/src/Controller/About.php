<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;
    use Drupal\Core\Controller\ControllerBase;
    use Symfony\Component\HttpFoundation\Request;
    use Drupal\Core\Database\Database;
    use Drupal\Core\Entity\Query\QueryFactory;

class About {

  public function page() {
    
  	// $content = array('testing contact us');
    return array(
        '#theme' => 'about_us',
        '#title'=>'About Page',
        '#items'=>''
        //'#title'=>'Our Article List'
    );
  }

}