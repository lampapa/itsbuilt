<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */
//use Drupal\taxonomy\Entity\Term;
namespace Drupal\drupalup_controller\Controller;
use Drupal\taxonomy\Entity\Term;
/*require_once 'sites/libraries/spreadsheet-reader-master/SpreadsheetReader.php';
require_once 'sites/libraries/spreadsheet-reader-master/php-excel-reader/excel_reader2.php';*/

class Brands {

  public function page() {
   
    $service_array =[];
    
    /*$targetPath = 'example.xls';
    
        $Reader = new \SpreadsheetReader($targetPath);
        
        $sheetCount = count($Reader->sheets());
      
        for($i=0;$i<$sheetCount;$i++)
        {
            $Reader->ChangeSheet($i);
            
            foreach ($Reader as $Row)
            {
              echo ($Row[0]);
              die();
               
             }
        
         }
 */



    $vid = 'brand';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
      $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
      $url = "";
      if(isset($term_obj->get('field_brand_image')->entity)){
        $url = file_create_url($term_obj->get('field_brand_image')->entity->getFileUri());
      }
      $service_array[] = array(
        $term->name,
        $term_obj->get('field_urlaliaspaths')->value,
        $url,
        $term->tid,
        $term_obj->field_brand_image->alt
      );
    } 
    
    return array(
    '#theme' => 'brands',
    '#items'=>$service_array
    //'#title'=>'Our Article List'
    );
  }

}