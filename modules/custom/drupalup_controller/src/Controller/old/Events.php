<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */
//use Drupal\taxonomy\Entity\Term;
namespace Drupal\drupalup_controller\Controller;
use Drupal\taxonomy\Entity\Term;

class Events {
    public function __construct(){
    $this->connection = \Drupal::database();    
  }
 
  public function page() {   
    $service_array =[];    
    
            $nids          = \Drupal::entityQuery('node')->condition('type','homepage_event')->execute();
           foreach($nids as $key => $ids){               
            $node = \Drupal\node\Entity\Node::load($ids);            
            $res = $node->field_sequence_event->getValue();
            
            $service_array[] = array(
            'title'=>$node->getTitle(),
            'raw_html'=>$node->body->value,
            'detailval'=>$node->field_homepage_event_data->value,         
           'sequence'=>$res[0]['value'],
            'id'=>$ids
            );
          }

      
    usort($service_array, function($a, $b) {
          return $a['sequence'] <=> $b['sequence'];
    });
    
    return array(
      '#theme' => 'events',
      '#items'=>$service_array
    );
  }

}