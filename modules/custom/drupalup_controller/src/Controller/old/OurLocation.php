<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;

class OurLocation {

  public function page() {

    $service_array = [];
    $seo_array=[];
    $nids = \Drupal::entityQuery('node')->condition('type','location_info')->execute();
    foreach ($nids as $key => $ids) {
      $node = \Drupal\node\Entity\Node::load($ids);   
      $service_array[] = array(
      $node->body->value,$ids
      );
    } 
    /*print_r($service_array);
    exit;*/
    return array(
    '#theme' => 'our_location',
    '#items'=>$service_array,
    '#seo_array' => $seo_array
    //'#title'=>'Our Article List'
    );
  }

}

?>