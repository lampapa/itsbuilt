<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;

class Home {

  public function page() {
    
  	// $content = array('testing contact us');
    return array(
        '#theme' => 'home',
        '#title'=>'Home Page'
        // '#items'=>$items
        //'#title'=>'Our Article List'
    );
  }

}