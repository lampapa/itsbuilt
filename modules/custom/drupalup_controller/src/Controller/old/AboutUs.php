<?php
    /**
     * @file
     * Contains \Drupal\hello\HelloController.
     */

    namespace Drupal\drupalup_controller\Controller;

    class AboutUs {

      public function page() {  	
        $service_array ='';
        $connection = \Drupal::database();
        $nids = \Drupal::entityQuery('node')->condition('type','about_us')->execute();
        foreach ($nids as $key => $ids) {
          $node = \Drupal\node\Entity\Node::load($ids); 
          $service_array =$node->body->value; 
                  
        } 

       
       
        /*usort($service_array, function($a, $b) {
            return $a['sequence'] <=> $b['sequence'];
        });*/
        $seo_array=[];
        $query = $connection->query("SELECT h1_tag,h2_tag FROM catapult_seo where page_name='About'");
        while ($row = $query->fetchAssoc()){      
           $seo_array[] = array($row['h1_tag'],$row['h2_tag']);
        }

      
        return array(
        '#theme' => 'about_us',
        '#items'=>$service_array,
        '#seo_array' => $seo_array
        //'#title'=>'Our Article List'
        );
      }

    }

?>
