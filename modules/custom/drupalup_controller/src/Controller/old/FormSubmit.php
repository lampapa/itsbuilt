<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;

class FormSubmit {

  public function page() {
    	$form = \Drupal::formBuilder()->getForm('Drupal\amazing_forms\Form\ContributeForm');  	
    return array(
        '#theme' => 'form_submit',
        '#items'=>$form
        //'#title'=>'Our Article List'
    );
  }

}