<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;

class UserOrders {

  public function page() {
    $user        = \Drupal::currentUser();
    $user_id     = $user->id();
    $connection  = \Drupal::database();
    $orders_array=[];
    $query = $connection->query("SELECT co.order_id,co.order_number,co.store_id,co.uid,co.mail,ROUND(co.total_price__number,2) AS total_price__number ,co.total_price__currency_code,co.state AS paymentstatus,DATE_FORMAT(FROM_UNIXTIME(co.created), '%e %b %Y') AS orderdate,co.changed AS modifieddate,co.placed AS orderplaceddate,co.completed AS ordercompleteddate,oi.title AS productname,oi.quantity,oi.unit_price__number,oi.unit_price__currency_code FROM commerce_order AS co LEFT JOIN commerce_order_item AS oi ON oi.order_id=co.order_id WHERE co.uid='".$user_id."'");
    while ($row = $query->fetchAssoc()) {  
      $orders_array[] = $row;
    }
   
    return array(
        '#theme' => 'user_orders',
        '#ordersinfo'=>$orders_array
        //'#title'=>'Our Article List'
    );
  }
  /*public function getuserorders(){
    $user        = \Drupal::currentUser();
    $user_id     = $user->id();
    $connection  = \Drupal::database();
    $query = $connection->query("SELECT co.order_id,co.order_number,co.store_id,co.uid,co.mail,ROUND(co.total_price__number,2) AS total_price__number ,co.total_price__currency_code,co.state AS paymentstatus,DATE_FORMAT(FROM_UNIXTIME(co.created), '%e %b %Y') AS orderdate,co.changed AS modifieddate,co.placed AS orderplaceddate,co.completed AS ordercompleteddate,co.payment_gateway,oi.title AS productname,oi.quantity,oi.unit_price__number,oi.unit_price__currency_code FROM commerce_order AS co LEFT JOIN commerce_order_item AS oi ON oi.order_id=co.order_id WHERE co.uid='".$user_id."' and co.state !='draft' " );
    while ($row = $query->fetchAssoc()) {  
      
      $array_request[] = $row['order_id'];
      $count = count($array_request);
        if($count == 1){
          $last_value = "";
          $last_before = "";
        }else{
          $last_value = end($array_request);
          $last_before = $array_request[count($array_request)-2];
        } 
        if(($last_value != $last_before) || $count == 1){ 
            $orders_array[] = $row;
        }
    }

     $data['order_results'] =$orders_array;
    echo json_encode($data);
    exit;
  }*/
  public function getuserorders(){
      global $base_url;
      $uid = \Drupal::currentUser()->id(); 
      $order_id = '';$total_price ='';
      $orderdate ='';$payment_gateway='';
      $paid_status ='';$orders=[];
      $c_value = "";
      $color_name = "";
      $c_url = "";

    /* $query1 = db_query("SELECT order_id,`total_price__number`,payment_gateway,state,DATE_FORMAT(FROM_UNIXTIME(created), '%e %b %Y') AS orderdate FROM `commerce_order` WHERE uid=".$uid." AND state != 'draft'");*/
      $query1 = db_query("SELECT order_id,`total_price__number`,payment_gateway,IFNULL(product_status,'Placed') AS state,DATE_FORMAT(FROM_UNIXTIME(created), '%e %b %Y') AS orderdate FROM `commerce_order` WHERE uid=".$uid." AND cart=0 AND state='completed'");
      /*$qcount = count($query1);
      print_r($qcount);*/
     $order_items = array();
       if(!empty($query1)){  
        while($res = $query1->fetchAssoc()){
          $order_id = $res['order_id'];
          $payment_gateway = $res['payment_gateway'];
          $paid_status = $res['state'];
          $orderdate = $res['orderdate'];
          $total_price = number_format($res['total_price__number'],2);
          $query = db_query("SELECT cp.product_id,oi.title,oi.quantity,oi.unit_price__number,oi.total_price__number,oi.purchased_entity,fm.uri FROM commerce_product_variation_field_data AS cp 
            LEFT JOIN commerce_order_item AS oi ON cp.variation_id = oi.`purchased_entity`
            LEFT JOIN commerce_product__field_product_image AS pim ON pim.entity_id =cp.product_id LEFT JOIN file_managed AS fm ON fm.fid =pim.field_product_image_target_id WHERE oi.order_id =".$order_id."  ORDER BY cp.product_id ASC");
            $orders = array();
            while($row = $query->fetchAssoc()){
              $spliturl=str_replace('public://','',$row['uri']);    
              $prodname = mb_strimwidth($row['title'], 0, 30, "..."); 
              $p_id = $row['product_id'];
              $current_title = $this->get_title($p_id);
              $variant_id = $row['purchased_entity'];
              $p_name =$row['title'];
              $p_price =round($row['unit_price__number'],2);
              $p_quantity =intval($row['quantity']);
              //$order_id   = $row['order_id'];
              $image = $base_url."/sites/default/files/".$spliturl;
              $urls = $base_url."/checkout/".$order_id."/orderinformation";
              $p_totalprice = $p_price*$p_quantity;

              //color variant picking
              ///// modified by nandha //// 
              $qry_variation=db_query("SELECT cp.variation_id,cp.price__number as price_number,cp.price__currency_code as price_currency_code,cc.name as color_name,tp.value as c_value,tp.img_url as c_url FROM commerce_product_variation_field_data AS cp LEFT JOIN commerce_product_variation__field_color AS pn ON pn.entity_id=cp.variation_id LEFT JOIN taxonomy_term_field_data AS cc ON cc.tid=pn.field_color_target_id LEFT JOIN catapult_color_filter as tp on tp.name = cc.name
              where cp.product_id=".$p_id." AND cp.variation_id=".$variant_id." AND cp.status = 1 AND cc.status = 1 ");
              $vari_filter = [];
              while($row = $qry_variation->fetchAssoc()){
                $price_number = $row['price_number'];
                $price_currency_code = $row['price_currency_code'];
                $color_name = $row['color_name'];
                $c_value = $row['c_value'];
                $c_url = $row['c_url'];
              }

              /*$color_name*/
              $vid   = 'color_parent';
              $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid); 
              
              foreach ($terms as $term=>$val){

                  if($val->name ==  $color_name){
                     $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($val->tid);
                     $clr_code = $term_obj->get('field_colorname')->value;
                   
                  }
              }
              

              $orders[] =array(
                'spliturl' =>$spliturl ,
                'product_id' =>$p_id ,
                'unit_price' =>$p_price ,
                'quantity' =>$p_quantity ,
                'price' =>$p_totalprice ,
                'variant_id' =>$variant_id ,
                'color_name' =>$color_name ,
                'c_value'=>$c_value,
                'c_url'=>$c_url,
                'title'=>$current_title,
                'clr_code' => $clr_code
              ); 

            }
            $order_items[]= array('order_number' =>$order_id ,'total_price' =>$total_price,'orderdate'=>$orderdate,'payment_gateway'=>$payment_gateway ,'paymentstatus'=>$paid_status,'orders'=>$orders,'placed'=>'' );
          }  

        } 
      //echo '<pre>';  
      //print_r($order_items);
        $data['order_results'] = $order_items;
      echo json_encode($data);
      exit();
  }

  public function get_title($id){
    $res = db_query("SELECT title FROM `commerce_product_field_data` WHERE product_id=".$id)->fetchAssoc();
    return $res['title'];
  }


}