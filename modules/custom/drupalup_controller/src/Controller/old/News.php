<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */
//use Drupal\taxonomy\Entity\Term;
namespace Drupal\drupalup_controller\Controller;
use Drupal\taxonomy\Entity\Term;

class News {
    public function __construct(){
    $this->connection = \Drupal::database();    
  }
 
  public function page() {   
    $service_array =[];    
    if(isset($_GET['category_id']))
    {       
         $query = $this->connection->query(" select nid,title,body_value,field_homepage_news_data_value,field_sequence_news_value from( select *  from(
                 SELECT entity_id news_id FROM opticat.node__field_news_cat where field_news_cat_value='".trim($_GET['category_id'])."')a 
                 left join (select nid,title from node_field_data where type='homepage_news' ) b on nid=news_id 
                 left join (SELECT  entity_id news_id1,field_homepage_news_data_value FROM  node__field_homepage_news_data) c on nid=news_id1
                 left join (SELECT  entity_id news_id2,field_sequence_news_value FROM  node__field_sequence_news) d on nid=news_id2
                 left join (SELECT  entity_id news_id3,body_value FROM  node__body) f on nid=news_id3 )g order by field_sequence_news_value asc");
         while ($row = $query->fetchAssoc()) 
               {                 
                    $service_array[] = array(
                                      'title'=>$row['title'],
                                      'raw_html'=>$row['body_value'],
                                      'detailval'=>$row['field_homepage_news_data_value'],         
                                      'sequence'=>$row['field_sequence_news_value'],
                                      'id'=>$row['nid']
                                   );
               }



    } 
    else if(isset($_GET['year']) && isset($_GET['month']))
    {
          $date_det=$_GET['year'].'-'.$_GET['month'];
          $query = $this->connection->query(" select nid,title,body_value,field_homepage_news_data_value,field_sequence_news_value from( select *  from(
            SELECT entity_id news_id  FROM opticat.node__field_news_date where date_format(field_news_date_value,'%Y-%m')='".trim($date_det)."')a 
            left join (select nid,title from node_field_data where type='homepage_news' ) b on nid=news_id 
            left join (SELECT  entity_id news_id1,field_homepage_news_data_value FROM  node__field_homepage_news_data) c on nid=news_id1
            left join (SELECT  entity_id news_id2,field_sequence_news_value FROM  node__field_sequence_news) d on nid=news_id2
            left join (SELECT  entity_id news_id3,body_value FROM  node__body) f on nid=news_id3 )g order by field_sequence_news_value asc");
           while ($row = $query->fetchAssoc()) 
                 {                 
                    $service_array[] = array(
                                  'title'=>$row['title'],
                                  'raw_html'=>$row['body_value'],
                                  'detailval'=>$row['field_homepage_news_data_value'],         
                                  'sequence'=>$row['field_sequence_news_value'],
                                  'id'=>$row['nid']
                               );
                  }

    }
    else
    {
            $nids          = \Drupal::entityQuery('node')->condition('type','homepage_news')->execute();
           foreach($nids as $key => $ids){               
            $node = \Drupal\node\Entity\Node::load($ids);            
            $res = $node->field_sequence_news->getValue();
            
            $service_array[] = array(
            'title'=>$node->getTitle(),
            'raw_html'=>$node->body->value,
            'detailval'=>$node->field_homepage_news_data->value,         
            'sequence'=>$res[0]['value'],
            'id'=>$ids
            );
          }

    }    
    usort($service_array, function($a, $b) {
          return $a['sequence'] <=> $b['sequence'];
    });
    
    return array(
      '#theme' => 'news',
      '#items'=>$service_array
    );
  }

}