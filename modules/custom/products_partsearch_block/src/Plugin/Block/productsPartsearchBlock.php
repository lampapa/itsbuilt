<?php

/**
 * @file
 * Contains \Drupal\products_partsearch_block\Plugin\Block
 */

namespace Drupal\products_partsearch_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "products_partsearch_block",
 *  admin_label = @Translation("Products Partsearch Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class ProductsPartsearchBlock extends BlockBase{
 
   public function build(){
   
      return [
      '#theme' => 'productspartsearchtemplate',
      '#test_var' => $this->t('Test Value'),
    ];
  }
}

?>