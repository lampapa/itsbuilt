<?php

/**
 * @file
 * Contains \Drupal\brand_custom_block\Plugin\Block
 */

namespace Drupal\brand_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "brand_custom_block",
 *  admin_label = @Translation("Brand custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class BrandCustomBlock extends BlockBase{

   	public function build(){
	   	$brand_array =[];
	    /*$nids = \Drupal::entityQuery('node')->condition('type','brands')->execute();
	    foreach ($nids as $key => $ids) {
	      $node = \Drupal\node\Entity\Node::load($ids);
	      if(isset($node->field_brand_name->target_id)){
	      	$target_id = $node->field_brand_name->target_id;
	      }else{
	      	$target_id = "";
	      } 
	      $brand_array[] = array(
	      file_create_url($node->field_brand->entity->getFileUri()),$target_id
	      );
	    } */

	    $vid = 'brand';
	    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
	    foreach ($terms as $term) {
	      $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
	      $url = "";
	      if(isset($term_obj->get('field_brand_image')->entity)){
	        $url = file_create_url($term_obj->get('field_brand_image')->entity->getFileUri());
	      }
	      $brand_array[] = array(
	        //$term->name,
	        //$term_obj->get('field_urlaliaspaths')->value,
	        $url,
	        $term->tid,
	        //$term_obj->field_brand_image->alt
	      );
	    } 


	    return [
	      '#theme' => 'brandtemplate',
	      '#test_var' => $this->t('Test Value'),
	      '#brandinfo' =>array_chunk($brand_array,6),
    	];
 	}
}

?>