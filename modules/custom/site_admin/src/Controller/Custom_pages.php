<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\Core\Link;
class Custom_pages{
	public function __construct(){
		$this->connection = \Drupal::database();		
	}
  	public function page(){
        $query = $this->connection->query("SELECT * FROM menu_link_content_data left join menu_link_content on menu_link_content_data.id = menu_link_content.id   where expanded=1 and parent is null");
        while ($row = $query->fetchAssoc()) {
            $menu_content="menu_link_content:".$row['uuid'];            
            $title=$row['title'];
			$service_array[] = array('menu_content'=>$menu_content,'title'=>$title);
		}
  		
  		/*echo "<pre>";
  		print_r($service_array);
  		die();*/
	  	global $base_url;  	
	  	$success_status = $upload_error = $error = $editor_validate = "";  	
		if(isset($_SESSION['postid']) ){
			if($_SESSION['postid'] == ""){
				$_SESSION['postid'] = rand(10,100);
			}
		}else{
			$_SESSION['postid'] = rand(10,100);
		}
	  	if(!empty($_POST)){
	
	      	if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
		        if($_SESSION['postid'] == $_POST['postid']){
		        	
			  		$editor1    = $_POST['editor1'];
			  		
			  		$title    = $_POST['title'];
			  		//print_r();die;
			  	   if($editor1 == "" || $editor1 == "<p><br></p>"){		  		
			  			$editor_validate = "Please Enter Content";
			  			
			  		}else{			  				
						$title    = $_POST['title'];
						$menu_name = $_POST['menu_name'];

						$menu_level = $_POST['menu_level'];
						$menu_level_two = $_POST['menu_level_two'];
						$editor1    = trim($_POST['editor1']);

				
						if($_POST['hidden_id'] != ""){
							
							/*start update node*/
				  			$node                         = Node::load($_POST['hidden_id']);	
				  			$node->body->value            = $editor1;
							$node->body->format           = 'full_html';
							$node->title                  = $title;							
							$node->save();
							/*end of node update*/							

							/*menu update */
							$node = \Drupal\node\Entity\Node::load($_POST['hidden_id']);
							$menu_id = $node->field_menu_id->value;								
							$menu_link = MenuLinkContent::load($menu_id);
							$menu_link->title = $menu_name;
                             
                             if($_POST['menu_level']  != "" && $_POST['menu_level_two']  == "")
                             {
                             	$menu_link->parent = $menu_level;
                             }
                             else
                             {
                                $menu_link->parent = $menu_level_two;
                             }
							
							$menu_link->save();
							/*end of menu update */

							$success_status = "Custom Pages Updated Successfully";
				  		}else{		


				  			/*node create*/

				  			$node = Node::create([
										'type'  => 'custom',
										'title'	=> $title,																		
										'body'	=> ['value'=> $editor1,'format'=> 'basic_html']									
									]);
							$node->save();
							$node_id = $node->id();

							/*end of node create */

							/*menu create */

                            if($_POST['menu_level']  != "" && $_POST['menu_level_two']  == ""){
	                            $menu_link = MenuLinkContent::create([
								    'title' => $menu_name,
								    'link' => ['uri' => 'internal:/node/' . $node_id],
								    'menu_name' => 'main',						    
								     'expanded' => FALSE,
								    'parent' => $menu_level
								 ]);					  		
					  	     }else{                               
                               if($_POST['menu_level_two']  != ""){
                               	//	echo "test";
                               	//	die();
                                   $menu_link = MenuLinkContent::create([
								    'title' => $menu_name,
								    'link' => ['uri' => 'internal:/node/' . $node_id],
								    'menu_name' => 'main',						    
								     'expanded' => TRUE,
								    'parent' => $menu_level_two
							  		]);
                               }
                               else{
	                               $menu_link = MenuLinkContent::create([
								    'title' => $menu_name,
								    'link' => ['uri' => 'internal:/node/' . $node_id],
								    'menu_name' => 'main',						    
								     'expanded' => TRUE,
								  ]);
                              	}					  	     
					  	     }

							  $menu_link->save();
							  $menu_id = $menu_link->id();
							/*end of menu create */  


							/*update menu id to node id*/

							$node  = Node::load($node_id); 
							$node->field_menu_id = $menu_id;
							$node->body->value            = $editor1;
							$node->body->format           = 'full_html';
							$node->title                  = $title;
						 	$node->save();

						 	/*end of update node to menu id*/

							$success_status = "Custom Pages Added Successfully";			  			
						}
					}	
				}
			}	
			$_SESSION['postid'] = "";	
	  	} 
	  	if($_SESSION['postid'] == ""){
			$_SESSION['postid'] = rand(10,100);      
		} 	
			if($editor_validate != ""){
		$error = $editor_validate;
	}
	    return array('#theme' => 'custom_pages',
	    			 '#title' => $success_status,
	    			 '#postid'=>$_SESSION['postid'],
	    			 '#error'=> $error,
	    			 '#items'=>$service_array
					);
  	}

  	public function Custom_pages_url(){ 
  	 $connection = \Drupal::database(); 	
  	 $service_array =[];

	  	$nids          = \Drupal::entityQuery('node')->condition('type','custom')->execute();

	  	foreach($nids as $key => $ids){

	  	//	print_r($nids);die;
			$node = \Drupal\node\Entity\Node::load($ids);			

			$menu_id =$node->field_menu_id->value;
			//print_r($menu_id);die;
            $query = $connection->query("SELECT * FROM menu_link_content_data where id='".$menu_id."' ");     
            $row = $query->fetchAssoc();
            $menu_name=$row['title'];
            $service_array[] = array(
							        'title'=>$node->getTitle(),
							        'body'=>strip_tags($node->body->value),						        
							        'menu_name'=> $menu_name,
							        'id'=>$ids
	        						);
		}	

		return new JsonResponse([
	     $service_array
	   ]);

  	}


   public function singlecustom(){     
      	$connection = \Drupal::database(); 	
        $id   = $_POST['id'];
	  	$node = \Drupal\node\Entity\Node::load($id);
        $menu_id =$node->field_menu_id->value;		
        $query = $connection->query("select * from ( select title,parent,SUBSTRING(parent,'19') pid FROM menu_link_content_data where id='".$menu_id."' ) a      left join (SELECT id,uuid FROM opticat.menu_link_content) b on pid=uuid ");     
        $row = $query->fetchAssoc();
        $menu_name=$row['title'];
        $parent=$row['parent'];
        $parent_id=$row['id'];
        if($row['parent']!='')
          {          
              $query2 = $connection->query("select parent FROM menu_link_content_data where id='".$parent_id."'");     
              $row2 = $query2->fetchAssoc();
             if($row2['parent']!='')
              { 
                $parent_two=$parent;
                $parent=$row2['parent'];
              }
              else
              {
                $parent_two='';
                $parent=$parent;
              }             
          }
          else
          {          	 
             $parent=$parent;
             $parent_two='';    

          }
          
		$service_array[] = array(
						        'title'=>$node->getTitle(),
						        'body'=>$node->body->value,
						        'id'=>$id,						        
						        'menu_name'=>$menu_name,
						        'menu_level'=>$parent,
						        'menu_level_two'=>$parent_two						       
	    						);
		echo json_encode($service_array);
		exit();
  }


  	public function deletecustom()
  	{
      
	    $nodeid = $_POST['id'];      
        $res = array($nodeid=>$nodeid);		
        $node = \Drupal\node\Entity\Node::load($nodeid);
        $menu_id =$node->field_menu_id->value; 
       // $menu_link = \Drupal::entityTypeManager()->getStorage('menu_link_content')->load($menu_id);
       // $menu_link->delete();          
        $query = $this->connection->query("select * from ( select id id_one FROM opticat.menu_link_content_data where id='".$menu_id."'  ) a      left join (SELECT id id_two,uuid FROM opticat.menu_link_content) b on id_one=id_two ");            
        $query->allowRowCount = TRUE;
        $num_rows = $query->rowCount();
        $row = $query->fetchAssoc();     
      //  if($num_rows>0){ 
        $query_two = $this->connection->query(" select id,SUBSTRING(parent,'19') pid FROM menu_link_content_data where SUBSTRING(parent,'19')='".$row['uuid']."'  ") ; 
        $query_two->allowRowCount = TRUE;
        $num_rows_two = $query_two->rowCount();
                
            if($num_rows_two>0)
            { 
                    while ($rows = $query_two->fetchAssoc()) 
                    { 
                    	$query_three = $this->connection->query(" select entity_id FROM opticat.node__field_menu_id where field_menu_id_value='".$rows['id']."'  ") ; 
                       //$menu_link = \Drupal::entityTypeManager()->getStorage('menu_link_content')->load($rows['id']);
                     // $menu_link->delete();
                        $query_three->allowRowCount = TRUE;
                        $num_rows_three = $query_three->rowCount();
             
                        if($num_rows_three>0)
                        { 
                            while ($rowss = $query_three->fetchAssoc()) 
                                { 
                    	            $ress = array($nodeids=>$rowss['entity_id']);
		                            entity_delete_multiple('node', $ress);
	                            }
                        }
                    }
            }    //}
        entity_delete_multiple('node', $res);
		die();	   
    }

    public function menu_details(){    
        $menu_id = $_POST['id'];       
        $query = $this->connection->query("SELECT * FROM menu_link_content_data left join menu_link_content on menu_link_content_data.id = menu_link_content.id where parent='".$menu_id."' ");       
        while ($row = $query->fetchAssoc()) {
            $menu_content="menu_link_content:".$row['uuid'];            
            $title=$row['title'];
			$service_array[] = array('menu_content'=>$menu_content,'title'=>$title);
		}      
        echo json_encode($service_array);
		exit();
  	}
}