<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;

class ReviewSetting{
  public function page(){
  	 return array('#theme' => 'review_setting',
    			 '#title' => ""
    			
				);
  }

  public function reviewurl(){
  	  $connection = \Drupal::database();
  	  $review_array=[];
	  $query = $connection->query("SELECT id,name,email,rating,comment,product_id,part_no,flag FROM catapult_review");
	  while ($row = $query->fetchAssoc()) {
	  $review_array[] = array(
	      'id'=>$row['id'],
	      'name'=>$row['name'],
	      'email'=>$row['email'],
	      'rating'=>$row['rating'],
	      'comment'=>$row['comment'],
	      'product_id'=>$row['product_id'],
	      'part_no'=>$row['part_no'],
	      'flag'=>$row['flag']
	    );
	  }
	  echo json_encode($review_array);
	  exit();
  }

  public function publishreview(){

  	$user        = \Drupal::currentUser();
    $user_display_name = $user->getDisplayName();    
    $connection = \Drupal::database();
  	$result=$connection->query("update catapult_review set flag='".$_POST['status']."',modified_by='".$user_display_name."',modified_on=Now() where id='".$_POST['id']."'");
  	$result->allowRowCount = TRUE;
	$count = $result->rowCount();
	echo $count;
	exit();
  }

  public function deletereview(){
  	$connection = \Drupal::database();
  	$result=$connection->query("delete FROM catapult_review where id='".$_POST['id']."'");
  	$result->allowRowCount = TRUE;
	$count = $result->rowCount();
	echo $count;
	exit();
  }
  	
}