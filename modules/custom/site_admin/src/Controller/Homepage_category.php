<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;
use Symfony\Component\HttpFoundation\JsonResponse;
class Homepage_category{
  public function page(){

  	global $base_url;  	
  	$success_status = "";
  	$upload_error = "";
  	$error = "";
  	$editor_validate = "";
  	if(isset($_SESSION['postid']) ){
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);
      }
    }else{
      $_SESSION['postid'] = rand(10,100);
    }
	
  	if(!empty($_POST)){
      	if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
	        if($_SESSION['postid'] == $_POST['postid']){
		  		
		  		$category_name  = $_POST['category_name'];
		  		if($category_name == "" ){
		  			$editor_validate = "Please Enter category Name";
		  		}else{
					$title    = $_POST['head'];
					if($_POST['hidden_id'] != ""){
						/*echo "up";
						exit;*/
						$node                         = Node::load($_POST['hidden_id']);	
			  			
						$node->field_category_name_data->value = $category_name;
						$node->title                  = $title;
						$node->save();
						$success_status = "Category Updated Successfully";
						
			  		}else{
			  			/*echo "down";
						exit;*/
			  			$node = Node::create([
									'type'  => 'homepage_category_details',
									'title'	=> $title,
									'field_category_name_data' => $category_name,
									
									
								]);
						$node->save();
						$success_status = "Category Added Successfully";
			  			
					}
				}	
			}
		}	
		$_SESSION['postid'] = "";	
  	} 
  	if($_SESSION['postid'] == ""){
		$_SESSION['postid'] = rand(10,100);      
	} 
	if($upload_error != ""){
		$error = $upload_error;
	}
	if($editor_validate != ""){
		$error = $editor_validate;
	}

    return array('#theme' => 'homepage_category_setting',
    			 '#title' => $success_status,
    			 '#postid'=>$_SESSION['postid'],
    			 '#error'=> $error
				);
   // print_r(1);die;
  }

  	public function homepage_category_url(){  		
		$service_array =[];
	  	$nids          = \Drupal::entityQuery('node')->condition('type','homepage_category_details')->execute();
	  	foreach($nids as $key => $ids){
			$node = \Drupal\node\Entity\Node::load($ids);
			//$res = $node->field_category_sequence->getValue();			
			$service_array[] = array(
							        'title'=>$node->getTitle(),							    					        
							        'category_name'=>strip_tags($node->field_category_name_data->value),
							        'id'=>$ids,
							      
	        						);
		}		
		return new JsonResponse([
	      $service_array
	    ]);
  	}

  	public function homepage_category_delete(){
     
	    $nodeid = $_POST['id'];
	    $res = array($nodeid=>$nodeid);
		entity_delete_multiple('node', $res);
		die();
  }
  	public function homepage_category_edit(){
      	$id   = $_POST['id'];
	  	$node = \Drupal\node\Entity\Node::load($id);	
		$service_array[] = array(
						        'title'=>$node->getTitle(),						       
						        'id'=>$id,
						        'category_name'=>$node->get('field_category_name_data')->value,
						        						       
	    						);
		echo json_encode($service_array);
		exit();
  	}  
}