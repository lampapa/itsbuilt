<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\menu_link_content\Entity\MenuLinkContent;
class Homepage_topbar{
  public function page(){

  	/*$my_menu = \Drupal::entityTypeManager()->getStorage('menu_link_content')
    ->loadByProperties(['menu_name' => 'main']);
   
  foreach ($my_menu as $menu_item) {
  	
  	$title = $menu_item->getTitle();

   if($title == "Why Opticat"){
   
   
		  break;
    }
  
  	
    $parent_id = $menu_item->getParentId();
   
    if (empty($parent_id)) {
      $top_level = $menu_item->id();
    
    }
  }
 



  $menu_link = \Drupal::entityTypeManager()->getStorage('menu_link_content')->load(14);
  $menu_link->delete();*/
  	global $base_url;  	
  	$success_status = "";
  	$upload_error = "";
  	$error = "";
  	$editor_validate = "";
	if(isset($_SESSION['postid']) ){
		if($_SESSION['postid'] == ""){
			$_SESSION['postid'] = rand(10,100);
		}
	}else{
		$_SESSION['postid'] = rand(10,100);
	}
  	if(!empty($_POST)){
      	if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
	        if($_SESSION['postid'] == $_POST['postid']){
	        	//print_r($_POST);die;
		  		$value    = $_POST['editor1'];
		  		if($value == "" || $value == "<p><br></p>"){
		  			
		  			$editor_validate = "Please Enter Content";
		  		}else{
		  			//print_r($_POST);die;
					$title    = $_POST['head'];
					if($_POST['hidden_id'] != ""){

			  			$node                         = Node::load($_POST['hidden_id']);	
			  			$node->body->value            = $value;
						$node->body->format           = 'full_html';
						$node->title                  = $title;
						$node->save();
						$success_status = "Homepage Topbar Updated Successfully";
						
			  		}else{
			  			/*echo "down";
			  			exit;*/
			  			$node = Node::create([
									'type'  => 'homepage_topbanner',
									'title'	=> $title,
									'body'	=> ['value'=> $value,'format'=> 'basic_html']
									
								]);
						$node->save();
						
						$success_status = "Homepage Topbar Added Successfully";
			  			
					}
				}	
			}
		}	
		$_SESSION['postid'] = "";	
  	} 
  	if($_SESSION['postid'] == ""){
		$_SESSION['postid'] = rand(10,100);      
	} 
	if($editor_validate != ""){
		$error = $editor_validate;
	}
	
    return array('#theme' => 'Homepage_topbar',
    			 '#title' => $success_status,
    			 '#postid'=>$_SESSION['postid'],
    			 '#error'=> $error
				);
  }

  	public function homepagetop_url(){  		
		$service_array =[];
	  	$nids          = \Drupal::entityQuery('node')->condition('type','homepage_topbanner')->execute();
	  	foreach($nids as $key => $ids){
			$node = \Drupal\node\Entity\Node::load($ids);			
			$service_array[] = array(
							        'title'=>$node->getTitle(),
							        'body'=>$node->body->value,							        
							        'id'=>$ids
	        						);
		}		
		return new JsonResponse([
	      $service_array
	    ]);
  	}
}