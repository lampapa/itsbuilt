<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Session\AccountProxyInterface;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\commerce\commerce_product;
use Drupal\commerce;
use Drupal\commerce_cart;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CssCommand;

//for catlisting in header
//use Drupal\drupalup_controller\Controller;

class Paypl{

  public function page(){
  	$connection = \Drupal::database();
    global $base_url;  
      $success_status = "";
      $error = "";
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);      
    }
  if(!empty($_POST)){ 
  	if($_POST['paypal'] !=''){
  		
  		$displayname =$transactionmode = $merchantid = $email_invoice =$secretkey =$configid='';
      
      if(isset($_POST['displayname']) && !empty($_POST['displayname'])){
        $displayname     = $_POST['displayname'];

      }else{
       
        $error .=" Display Name";
      }
      if(isset($_POST['email_invoice']) && !empty($_POST['email_invoice'])){
        $email_invoice     = $_POST['email_invoice'];
      }else{
        $error .=",Email";
      }
      if(isset($_POST['transactionmode']) && !empty($_POST['transactionmode'])){
        $transactionmode     = $_POST['transactionmode'];
      }else{
        $error .=",Transaction Mode";
      }
      if(isset($_POST['merchantid']) && !empty($_POST['merchantid'])){
        $merchantid     = $_POST['merchantid'];
      }else{
        $error .=",MerchantID";
      }
      if(isset($_POST['secretkey']) && !empty($_POST['secretkey'])){
        $secretkey     = $_POST['secretkey'];
      }else{
        $error .=",SecretKey";
      }
      if(isset($_POST['hidden_id'])){
        $configid     = $_POST['hidden_id'];
      }
      if($configid ==''){
        if($displayname !='' && $transactionmode !='' && $merchantid !='' && $secretkey !='' && $email_invoice !=''){
          $connection->query("insert into catapult_paypal_config(display_name,email,transaction_mode,merchant_id,secret_key,created_by,created_on)values('".$displayname."','".$email_invoice."','".$transactionmode."','".$merchantid."','".$secretkey."','JP',Now())");  
        }  
      }else{
        if($displayname !='' && $transactionmode !='' && $merchantid !='' && $secretkey !='' && $email_invoice !=''){
           $connection->query("update catapult_paypal_config set display_name='".$displayname."',email='".$email_invoice."',transaction_mode='".$transactionmode."',merchant_id='".$merchantid."',secret_key='".$secretkey."',modified_by='JP',modified_on=Now() where config_id='".$configid."'"); 
           }  
      }
      unset($_POST);

  	}
    $_SESSION['postid'] = "";
  }
  if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);      
  }

    if(isset($error) && $error != '')
    {
      $error ="Please Fill The".$error;
    }

   /* echo  $error;
    exit;*/
    return array(
        '#theme' => 'paypal',
        '#title' => $success_status,
        '#postid'=>$_SESSION['postid'],
        '#error'=>$error
    );
  }
  public function getpaypalinfo() {
    $connection = \Drupal::database();
    $query = $connection->query("SELECT * FROM catapult_paypal_config");
    $paypal_results=[];
    while ($row = $query->fetchAssoc()) {  
       $paypal_results[] = $row;
    }
    $data['paypal_results'] =$paypal_results;
    echo json_encode($data);
    exit; 
  }
}
