<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;

class VideoSetting{
  	public function page(){
	  	global $base_url;
	  	$success_status = "";
	  	if(isset($_SESSION['postid']) ){
	      if($_SESSION['postid'] == ""){
	        $_SESSION['postid'] = rand(10,100);
	      }
	    }else{
	      $_SESSION['postid'] = rand(10,100);
	    }      	
	  	$upload_error = "";
	  	$upload_video_error = "";
	  	$error           = "";
	  	$editor_validate = "";
	  	if(!empty($_POST)){
	      	if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
		        if($_SESSION['postid'] == $_POST['postid']){		              	
					$title    = $_POST['heading'];			
					$alt      = $_POST['altext'];
					$sequence = $_POST['video_sequence']; 
					
					/*video image */
					if($_FILES["video_image"]["name"] != ""){
						$name = $_FILES["video_image"]["name"];
						$exts = explode(".", $name);
						$extension = $exts[1];
						$allowedExts = array("jpeg", "jpg", "png","PNG","JPEG",'jpg');
						if(in_array($extension, $allowedExts)){
							$target_file =  basename($_FILES["video_image"]["name"]);
							move_uploaded_file($_FILES["video_image"]["tmp_name"], $target_file);
							$data = file_get_contents($_FILES["video_image"]["name"]);
							$file = file_save_data($data, "public://".$_FILES["video_image"]["name"], FILE_EXISTS_REPLACE);
						}else{
							$upload_error = "Image Type Should Be jpg,png";
						}						
					}
					/*end of image*/

					/*video fileimage */
					if($_FILES["video_file"]["name"] != ""){							
						$name = $_FILES["video_file"]["name"];
						$exts = explode(".", $name);
						$extension = $exts[1];
						$allowedExts = array("mp4", "MP4");
						if(in_array($extension, $allowedExts)){
							$target_file =  basename($_FILES["video_file"]["name"]);
							move_uploaded_file($_FILES["video_file"]["tmp_name"], $target_file);
							$data = file_get_contents($_FILES["video_file"]["name"]);
							$file1 = file_save_data($data, "public://".$_FILES["video_file"]["name"], FILE_EXISTS_REPLACE);
						}else{
							$upload_video_error = "Video Should Be MP4";
						}	
					}

					/*end of video file*/

			  		if($_POST['hidden_id'] != ""){
			  			$node                         = Node::load($_POST['hidden_id']);					
						$node->title                  = $title;
						$node->field_video_sequence->value  = $sequence;
						if($upload_error == ""){
							if( ($_FILES["video_image"]["name"] != "") ){			
								$field_video_image = array(
								    'target_id' => $file->id(),
								    'alt'       => $alt,
								    'title'     => "My title"
								);
								$node->field_video_image = $field_video_image;
							}
						}
						if($upload_video_error == ""){		
							if($_FILES["video_file"]["name"] != ""){
								$field_video_file = array(
								    'target_id' => $file1->id(),
								    'title'     => "My title"
								);
								$node->field_video_file = $field_video_file;
							}
						}	
						if( ($upload_video_error == "") && ($upload_error == "") ){										
							$node->save();
							$success_status = "Video Updated Successfully";
						}	
			  		}else{
			  			if( ($_FILES["video_image"]["name"] != "") && ($_FILES["video_file"]["name"] != "") && ($upload_error == "") && ($upload_video_error == "")){
			  				$node = Node::create([
								'type'  => 'videos',
								'title'	=> $title,
								
								'field_video_sequence' => $sequence,
							  	'field_video_image' => [
							    'target_id' => $file->id(),
							    'alt' => $alt,
							    'title' => 'Sample File'
							  ],'field_video_file' => [
							    'target_id' => $file1->id(),
							    'alt' => $alt,
							  ],
							]);
							$node->save();
							chmod($_FILES["video_image"]["name"],0777);
							chmod($_FILES["video_file"]["name"],0777);
							unlink($_FILES["video_file"]["name"]);				
							unlink($_FILES["video_image"]["name"]);
							$success_status = "Video Added Successfully";	
			  			}else{
			  				$error = "Please Upload Valid File";
			  			}		   							
					}			  	 		
		      	}	      			     
			} 
			$_SESSION['postid'] = "";
		}	
		if($_SESSION['postid'] == ""){
	  		$_SESSION['postid'] = rand(10,100);      
		}  
		if($upload_error != ""){
			$error = $upload_error;			
		} 	  	
		if($upload_video_error != ""){
			$error = $upload_video_error;			
		} 
	   return array('#theme' => 'video_setting',
    				 '#title' => $success_status,
    				 '#postid'=>$_SESSION['postid'],
    				 '#error'=>$error
    				);
  	}
  	public function getvideos(){
		$connection  = \Drupal::database();
      	$user        = \Drupal::currentUser();
      	$user_id     = $user->id();
     	$query       = $connection->query("SELECT * FROM catapult_video_category");
        while($row = $query->fetchAssoc()){        		
        	$product1 = array("id"=>$row['id'],"videocategory"=>$row['video_category']);
        	$products[] = $product1;
        }
        echo json_encode($products);
        exit();       
  	}
  	public function getfullvideos(){ 	
		$service_array =[];
	  	$nids          = \Drupal::entityQuery('node')->condition('type','videos')->execute();
	  	/*echo "<pre>";
	  	print_r($nids);*/
	  	foreach($nids as $key => $ids){	  		
  			$node = \Drupal\node\Entity\Node::load($ids);				
			$res = $node->field_video_sequence->getValue();	
			if(empty($res)){
				$res = "";
			}else{
				$res = $res[0]['value'];
			}		
			$service_array[] = array(
						        'title'=>$node->getTitle(),
						        'body'=>strip_tags($node->body->value),
						        'file'=>file_create_url($node->field_video_image->entity->getFileUri()),
						        'sequence'=>$res,
						        'id'=>$ids
        						);
					
		}
		echo json_encode($service_array);	
		exit();	
  	}
  	public function deletevidoes(){ 
	  	$nodeid = $_POST['id'];	  		  	
		$result = \Drupal::entityQuery('node')
	      ->condition('type', 'videos')
	      ->execute();			     
	  	$res = array($nodeid=>$nodeid);
		entity_delete_multiple('node', $res);
		die();
  	}
    public function singlevideo(){ 
	  	$id   = $_POST['id'];
	  	$node = \Drupal\node\Entity\Node::load($id);
		$res  = $node->field_video_sequence->getValue();		
		$service_array[] = array(
						        'title'=>$node->getTitle(),
						        'body'=>strip_tags($node->body->value),
						        'file'=>file_create_url($node->field_video_image->entity->
						        	getFileUri()),
						        'file1'=>file_create_url($node->field_video_file->entity->getFileUri()),
						        'sequence'=>$res[0]['value'],
						        'id'=>$id,
						        'altvalue'=>$node->field_video_image->alt
	    						);
		echo json_encode($service_array);
		exit();
	}
	public function addtopics(){
		$topics      = $_POST['topics'];
		$connection  = \Drupal::database(); 		    
     	$query       = $connection->query("insert into catapult_video_category(video_category) values('".$topics."')");        
        exit();       
  	}
  	public function deletetopics(){
		$id          = $_POST['ids'];
		$connection  = \Drupal::database();
     	$query       = $connection->query("delete from catapult_video_category where id='".$id."'");
        exit();
  	}
}