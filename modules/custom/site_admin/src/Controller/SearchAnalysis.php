<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;

class SearchAnalysis{
	public function page(){
		return array('#theme' => 'search_analysis');
	}

	public function index(){
    $intervl = $_POST['intervl'];
    $connection  = \Drupal::database();

  		$query = $connection->query("SELECT keyword , COUNT(keyword) AS searchcount FROM catapult_keyword_search
									WHERE result_count  <> 0 
									AND (DATE_FORMAT(created_on, '%Y-%m-%d')) >= (DATE(NOW() - INTERVAL ".$intervl." DAY))
									GROUP BY keyword
									ORDER BY COUNT(keyword) DESC  LIMIT 5");
        $first = $query->fetchAll();

        $query1 = $connection->query("SELECT keyword , COUNT(keyword) AS searchcount FROM catapult_keyword_search
									WHERE result_count  = 1
									AND (DATE_FORMAT(created_on, '%Y-%m-%d')) >= (DATE(NOW() - INTERVAL ".$intervl." DAY))
									GROUP BY keyword
									ORDER BY COUNT(keyword) DESC  LIMIT 5");
        $second = $query1->fetchAll();
        
        $query2 = $connection->query("SELECT keyword , COUNT(keyword) AS searchcount FROM catapult_keyword_search
									WHERE result_count  = 0
									AND (DATE_FORMAT(created_on, '%Y-%m-%d')) >= (DATE(NOW() - INTERVAL ".$intervl." DAY))
									GROUP BY keyword
									ORDER BY COUNT(keyword) DESC  LIMIT 5");
        $third = $query2->fetchAll();

        $query3 = $connection->query("SELECT keyword , COUNT(keyword) AS searchcount FROM catapult_keyword_search
									WHERE result_count  > 1
									AND (DATE_FORMAT(created_on, '%Y-%m-%d')) >= (DATE(NOW() - INTERVAL ".$intervl." DAY))
									GROUP BY keyword
									ORDER BY COUNT(keyword) DESC  LIMIT 5");
        $fourth = $query3->fetchAll();



        $data = array('topsearches'=>$first,'oneresult'=>$second,'zeroresult'=>$third,'morethanone'=>$fourth);
       
        echo json_encode($data);
        die();
  }

  public function search_analysis_datepicker(){
    $from = $_POST['from'];
    $to = $_POST['to'];
    $connection  = \Drupal::database();

  		$query = $connection->query("SELECT keyword , COUNT(keyword) AS searchcount FROM catapult_keyword_search
									WHERE result_count  <> 0 
									AND (DATE_FORMAT(created_on, '%Y-%m-%d')) BETWEEN '".$from."'  AND '".$to."'
									GROUP BY keyword
									ORDER BY COUNT(keyword) DESC  LIMIT 5");
        $first = $query->fetchAll();

        $query1 = $connection->query("SELECT keyword , COUNT(keyword) AS searchcount FROM catapult_keyword_search
									WHERE result_count  = 1
									AND (DATE_FORMAT(created_on, '%Y-%m-%d')) BETWEEN '".$from."'  AND '".$to."'
									GROUP BY keyword
									ORDER BY COUNT(keyword) DESC  LIMIT 5");
        $second = $query1->fetchAll();
        
        $query2 = $connection->query("SELECT keyword , COUNT(keyword) AS searchcount FROM catapult_keyword_search
									WHERE result_count  = 0
									AND (DATE_FORMAT(created_on, '%Y-%m-%d')) BETWEEN '".$from."'  AND '".$to."'
									GROUP BY keyword
									ORDER BY COUNT(keyword) DESC  LIMIT 5");
        $third = $query2->fetchAll();

        $query3 = $connection->query("SELECT keyword , COUNT(keyword) AS searchcount FROM catapult_keyword_search
									WHERE result_count  > 1
									AND (DATE_FORMAT(created_on, '%Y-%m-%d')) BETWEEN '".$from."'  AND '".$to."'
									GROUP BY keyword
									ORDER BY COUNT(keyword) DESC  LIMIT 5");
        $fourth = $query3->fetchAll();



        $data = array('topsearches'=>$first,'oneresult'=>$second,'zeroresult'=>$third,'morethanone'=>$fourth);
       
        echo json_encode($data);
        die();
  }
}