<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Session\AccountProxyInterface;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\commerce\commerce_product;
use Drupal\commerce;
use Drupal\commerce_cart;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_price\Price;
use Drupal\taxonomy\Entity\Term;

class BuyAccessories{

    public function page(){
        global $base_url;
        $connection = \Drupal::database(); 
        $products = array();     
        $success_status = "";
        $error           = "";
        if(isset($_SESSION['postid']) ){
          if($_SESSION['postid'] == ""){
            $_SESSION['postid'] = rand(10,100);
          }
        }else{
          $_SESSION['postid'] = rand(10,100);
        }   
        if(!empty($_POST)){
            if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
                if($_SESSION['postid'] == $_POST['postid']){
                    if( (isset($_POST['hidden_id'])) && ($_POST['hidden_id'] != "")){
                        $id = $_POST['hidden_id'];
                        $partno = $_POST['partno'];                        
                        $hidden_alternate = $_POST['hidden_alternate'];               
                        $query = $connection->query("update catapult_buy_accessories set partno='".$partno."',alternative_partno='".$hidden_alternate."' where id='".$id."'"); 
                        $success_status = "Alternative Partno Updated Successfully";                        
                    }else{
                        $partno = $_POST['partno'];
                        $query       = $connection->query("SELECT count(id) as ids FROM catapult_buy_accessories where partno='".$partno."'");
                        while($row = $query->fetchAssoc()){             
                            $counts = $row['ids'];
                        }
                        if($counts > 0){
                            $error = "Partno Already Available";
                        }else{
                            $hidden_alternate = $_POST['hidden_alternate'];               
                            $query = $connection->query("insert into catapult_buy_accessories(partno,alternative_partno) values('$partno','$hidden_alternate')"); 
                            $success_status = "Alternative Partno Added Successfully";
                        }
                    }                     
                }
            }
            $_SESSION['postid'] = "";
        } 
        if($_SESSION['postid'] == ""){
            $_SESSION['postid'] = rand(10,100);      
        }  
        $query3 = $connection->query("SELECT product_id FROM commerce_product where delete_flag=0");                       
        while($row = $query3->fetchAssoc()){  
          $product      = \Drupal\commerce_product\Entity\Product::load($row['product_id']);           
          $partno      = $product->get('field_partno')->getValue()[0]['value'];      
          $partnos[] = $partno;
        }        
        return array(
            '#theme' => 'buy_accessories',
            '#partnos'=>$partnos,
            '#error'=>$error,
            '#title'=>$success_status,
            '#postid'=> $_SESSION['postid']     
        );
    }
    public function buyaccessoriesurl(){ 
        $array = [];       
        $connection  = \Drupal::database();
        $query       = $connection->query("SELECT * FROM catapult_buy_accessories");
        while($row = $query->fetchAssoc()){             
            $arrays[] = array('id'=>$row['id'],'access_partno'=>$row['partno'],'alternative_partno'=>$row['alternative_partno']);
        }
        echo json_encode($arrays);   
        exit(); 
    }
    public function deleteacessories(){
        $connection  = \Drupal::database();
        $id          = $_POST['id'];             
        $query       = $connection->query("delete FROM catapult_buy_accessories where id='".$id."'");    
        exit(); 
    }
     public function editacessories(){
        $connection  = \Drupal::database();
        $id          = $_POST['id'];             
        $query       = $connection->query("select * FROM catapult_buy_accessories where id='".$id."'"); 
        while($row = $query->fetchAssoc()){             
            $data[]=  array('id' =>$row['id'],'partno'=>$row['partno'],'alternative_partno'=>$row['alternative_partno'] );
        }  
        echo json_encode($data); 
        exit(); 
    }
}