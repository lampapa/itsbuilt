<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\menu_link_content\Entity\MenuLinkContent;
class Onsite{
  public function page(){

  	global $base_url;  	
  	$success_status = "";
  	$upload_error = "";
  	$error = "";
  	$editor_validate = "";
	if(isset($_SESSION['postid']) ){
		if($_SESSION['postid'] == ""){
			$_SESSION['postid'] = rand(10,100);
		}
	}else{
		$_SESSION['postid'] = rand(10,100);
	}
  	if(!empty($_POST)){
      	if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
	        if($_SESSION['postid'] == $_POST['postid']){
		  		$value    = $_POST['editor1'];
		  		if($value == "" || $value == "<p><br></p>"){
		  			$editor_validate = "Please Enter Content";
		  		}else{
					$title    = $_POST['head'];
					if($_POST['hidden_id'] != ""){

			  			$node                         = Node::load($_POST['hidden_id']);	
			  			$node->body->value            = $value;
						$node->body->format           = 'full_html';
						$node->title                  = $title;
						$node->save();
						$success_status = "Onsite Details Updated Successfully";
						
			  		}else{
			  			/*echo "down";
			  			exit;*/
			  			$node = Node::create([
									'type'  => 'onsite_details',
									'title'	=> $title,
									'body'	=> ['value'=> $value,'format'=> 'basic_html']
									
								]);
						$node->save();
						
						$success_status = "Onsite Details etails Added Successfully";
			  			
					}
				}	
			}
		}	
		$_SESSION['postid'] = "";	
  	} 
  	if($_SESSION['postid'] == ""){
		$_SESSION['postid'] = rand(10,100);      
	} 
	if($editor_validate != ""){
		$error = $editor_validate;
	}
    return array('#theme' => 'onsite',
    			 '#title' => $success_status,
    			 '#postid'=>$_SESSION['postid'],
    			 '#error'=> $error
				);
  }

  	public function onsite_url(){  		
		$service_array =[];
	  	$nids          = \Drupal::entityQuery('node')->condition('type','onsite_details')->execute();
	  	foreach($nids as $key => $ids){
			$node = \Drupal\node\Entity\Node::load($ids);			
			$service_array[] = array(
							        'title'=>$node->getTitle(),
							        'body'=>$node->body->value,							        
							        'id'=>$ids
	        						);
		}		
		return new JsonResponse([
	      $service_array
	    ]);
  	}
}