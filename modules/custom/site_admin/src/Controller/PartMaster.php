<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\commerce\commerce_product;
use Drupal\commerce;
use Drupal\commerce_cart;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_price\Price;
use Drupal\taxonomy\Entity\Term;
// require_once 'sites/libraries/spreadsheet-reader-master/SpreadsheetReader.php';
// require_once 'sites/libraries/spreadsheet-reader-master/php-excel-reader/excel_reader2.php';
require_once "sites/libraries/PHPExcel/Classes/PHPExcel.php";

class PartMaster{

  public function array_has_dupes($array) {
    $tags = implode(',',$array);
    $tag1 = implode(',',array_unique($array));
    if(sizeof($array) == 1){
      return ;
    }else{
      if($tags == $tag1 ) {
        return 0;
      }else{
        return 1;
      }
    }
  }

  public function page(){
    global $base_url;
    $upload_error       = "";
    $upload_video_error = "";
    $error              = "";
    $editor_validate    = "";
    $slider_error       = "";
    $slider_empty_error = "";
    $variation_error    = "";
    $color_array        = [];
    $sku_array          = [];
    $price_array        = [];
    $editor1            = '';
    $field_call_price   = '';
    $vid = 'brand';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
      $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
      $url = "";
      if(isset($term_obj->get('field_brand_image')->entity)){
        $url = file_create_url($term_obj->get('field_brand_image')->entity->getFileUri());
      }
      $brand[] = [
        'tid' => $term->tid,
        'tname' => $term->name
      ];
    }
    $vid   = 'color_parent';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
      $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
      $url = "";
      $colors[] = [
        'tid' => $term->tid,
        'tname' => $term->name
      ];
    }
    $success_status = "";
    if(isset($_SESSION['postid']) ){
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);
      }
    }else{
      $_SESSION['postid'] = rand(10,100);
    }    
    if(!empty($_POST)){
      if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
        if($_SESSION['postid'] == $_POST['postid']){
          $field_partno         = $_POST['field_partno'];
          $field_category_name  = $_POST['field_category_name'];
          $field_brand          = $_POST['field_brand'];
          $field_product_name   = $_POST['title'];
          $stock                = $_POST['stock'];
          $marketing_message    = $_POST['marketing_message'];
          $field_featured       = $_POST['field_featured'];
          $field_new            = $_POST['field_new'];
          $editor1              = $_POST['editor1'];
          $price                = $_POST['price'];
          $sku                  = $_POST['sku'];
          $field_total_price    = $_POST['field_total_price'];
          $field_lester         = $_POST['field_lester'];
          $field_interchange    = $_POST['field_interchange'];
          if(isset($_POST['field_call_price'])){
            $field_call_price     = $_POST['field_call_price'];
          }
          $emptysku             = [];
          $emptyprice           = [];
          $r                    = 0;

          /*start slider Validataion */
          /*if(!empty($_FILES['sliderimage'])){
            foreach($_FILES['sliderimage']['tmp_name'] as $key => $tmp_name){
              if($_FILES['sliderimage']['name'][$key] != ""){
                $file_name = $_FILES['sliderimage']['name'][$key];
                $name = $_FILES["sliderimage"]["name"][$key];
                $exts = explode(".", $name);
                $extension = $exts[1];
                $allowedExts = array("jpeg", "jpg", "png","PNG","JPEG",'jpg');
                if(in_array($extension, $allowedExts)){
                  move_uploaded_file($_FILES['sliderimage']['tmp_name'][$key],"public://digitalassetsimages/".$_FILES['sliderimage']['name'][$key]);
                  $data = file_get_contents("public://digitalassetsimages/".$_FILES["sliderimage"]["name"][$key]);
                  $slider_image_save = file_save_data($data, "public://upload_parts/".$_FILES["sliderimage"]["name"][$key], FILE_EXISTS_REPLACE);
                }else{
                  $slider_error = "Slider Image Type Should Be jpg,png";
                }
              }else{
                $slider_empty_error = "Slider Should not be empty";
              }
            }
          }*/

          if(!empty($_FILES['sliderimage']) ){
               // $targets = 0;
                foreach($_FILES['sliderimage']['tmp_name'] as $key => $tmp_name){
                  $file_name  = $_FILES['sliderimage']['name'][$key];
                 // $file_name1 = $_FILES['sliderimage']['name'];
                  if($file_name != ""){
                    $file_tmp =$_FILES['sliderimage']['tmp_name'][$key];
                    move_uploaded_file($file_tmp,"public://digitalassetsimages/".$file_name);
                    $data = file_get_contents("public://digitalassetsimages/".$file_name);
                    $file = file_save_data($data, "public://upload_parts/".$file_name, FILE_EXISTS_REPLACE);
                    $filew[] = array(
                        'target_id' => $file->id(),
                        'alt' => 'teee',
                        'title' => "My title"
                    );
                  }/*else{
                    $filew[] = array(
                        'target_id' => $target_id[$targets],
                        'alt' => 'teee',
                        'title' => "My title"
                    );
                  }*/
                  //$targets++;
                }
            }
          /*end of slider validation*/


          /*product image validation*/
          if($_FILES["product_image"]["name"] != ""){
            $name = $_FILES["product_image"]["name"];
            $exts = explode(".", $name);
            $extension = $exts[1];
            $allowedExts = array("jpeg", "jpg", "png","PNG","JPEG",'jpg');
            if(in_array($extension, $allowedExts)){
              // $target_file =  basename($_FILES["product_image"]["name"]);
              // move_uploaded_file($_FILES["product_image"]["tmp_name"], $target_file);
              // $data = file_get_contents($base_url."/".$_FILES["product_image"]["name"]);
              // $file_product = file_save_data($data, "public://".$_FILES["product_image"]["name"], FILE_EXISTS_REPLACE);
              move_uploaded_file($_FILES['product_image']['tmp_name'],"public://digitalassetsimages/".$_FILES['product_image']['name']);
              $data = file_get_contents("public://digitalassetsimages/".$_FILES["product_image"]["name"]);
              $file_product = file_save_data($data, "public://upload_parts/".$_FILES["product_image"]["name"], FILE_EXISTS_REPLACE);

            }else{
              $upload_error = "Product Image Type Should Be jpg,png";
            }
          }
          /*end of product image validation*/


          /*variation validation*/
            foreach($_POST['color'] as $key=>$valuecolor){
              $color_array[] = $valuecolor;
            }
            foreach($_POST['price'] as $key=>$valueprice){
              $price_array[] = $valueprice;

            }
            $color_errors = $this->array_has_dupes($color_array);
            //$price_array  = $this->array_has_dupes($price_array);

            if( ($color_errors  == 1)  ) {
              $variation_error = "variation Color should not be duplicate";
            }

          /*end of varation validation*/

          /*echo count($_POST['color']);
          die();*/
          foreach($_POST['color'] as $key=>$value){
            $variation_array[$r] = ['color'=>$value,'sku'=>$sku,'price'=>$_POST['price'][$key]];
            $r++;
          }
          $variation = '';
          $variation1 = '';
          if($_POST['hidden_id'] != ""){
            if( ($_FILES["product_image"]["name"] != "") && ($upload_error != "") ){
               $error = "Product Image Should Be jpg or png";
            }else if($slider_error != ""){
               $error = $slider_error;
            }else if($variation_error != ""){
               $error = $variation_error;
            }else{
              $node                           = product::load($_POST['hidden_id']);
              $node->body->value              = $editor1;
              $node->body->format             = 'full_html';
              $node->title                    = $field_product_name;
              $node->field_product_name       = $field_product_name;
              $node->field_partno             = $field_partno;
              $node->field_category_name      = $field_category_name;
              $node->field_featured           = $field_featured;
              $node->field_brand              = $field_brand;
              $node->field_stock_info         = $stock;
              $node->field_marketing_messages = $marketing_message;
              $node->field_new                = $field_new;
              $node->field_lester             = $field_lester;
              $node->field_interchange        = $field_interchange;
              $node->field_sku                = 15;
              $node->field_price              = 15;
              $node->field_call_price         = $field_call_price;
              $node->field_total_price        = new Price($field_total_price, 'USD');

              $node->variations = [];
              if($_FILES["product_image"]["name"] != ""){
                $field_product_image = array(
                    'target_id' => $file_product->id(),
                    'alt' => 'test',
                    'title' => "My title"
                );
                $node->field_product_image = $field_product_image;
              }
              $node->save();
              $product2 =Product::load($_POST['hidden_id']);


              /*$variation1   = ProductVariation::create([
                                          'type' => 'default',
                                          'sku' => $sku,
                                          'price' => new Price($field_total_price, 'USD'),
                                          'field_color'=>152
                                        ]);
              $variation1->save();
              $product2->addVariation($variation1);


              foreach($variation_array as $key=>$title_value){
                $prices       =  $variation_array[$key]['price'];
                $color        =  $variation_array[$key]['color'];
                $prices       =  $variation_array[$key]['price']+$field_total_price;
                $variation1   = ProductVariation::create([
                                          'type' => 'default',
                                          'sku' => $sku,
                                          'price' => new Price((string)$prices, 'USD'),
                                          'field_color'=>$color
                                        ]);
                $variation1->save();

                $product2->addVariation($variation1);
              }*/
              if(isset($_POST['setallcolors'])){
                $variation1   = ProductVariation::create([
                                            'type' => 'default',
                                            'sku' => $sku,
                                             'price' => new Price((string)$field_total_price, 'USD'),
                                            'field_color'=>152
                                          ]);
                $variation1->save();
                $product2->addVariation($variation1);
                $vid    = 'color_parent';
                $terms  = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
                foreach ($terms as $term) {

                  $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
                  $field_color_price_values = $term_obj->get('field_color_price_values')->value;                    
                  $prices =  $field_total_price+$field_color_price_values;                
                  $color  =  $term->tid; 


                  if($term->tid != 152){
                    $variation   = ProductVariation::create([
                                            'type' => 'default',
                                            'sku' => $sku,
                                            'price' => new Price((string)$prices, 'USD'),
                                            'field_color'=>$color
                                          ]);
                    $variation->save();
                    $product2->addVariation($variation);
                  }
                }
              }else{

                 /*default variation*/
                $variation1   = ProductVariation::create([
                                            'type' => 'default',
                                            'sku' => $sku,
                                             'price' => new Price((string)$field_total_price, 'USD'),
                                            'field_color'=>152
                                          ]);
                $variation1->save();
                $product2->addVariation($variation1);
                /*end of default variation*/

                foreach($variation_array as $key=>$title_value){
                  $prices =  $field_total_price+$variation_array[$key]['price'];
                  $color  =  $variation_array[$key]['color'];
                  $variation   = ProductVariation::create([
                                            'type' => 'default',
                                            'sku' => $sku,
                                            'price' => new Price((string)$prices, 'USD'),
                                            'field_color'=>$color
                                          ]);
                  $variation->save();
                  $product2->addVariation($variation);
                }
              }

              foreach( ($product2->get('field_image_one')->getValue()) as $key=>$value){
                $target_id[] = $value['target_id'];
              }
              if(!empty($_FILES['sliderimage']) ){
                $targets = 0;
                foreach($_FILES['sliderimage']['tmp_name'] as $key => $tmp_name){
                  $file_name  = $_FILES['sliderimage']['name'][$key];
                  $file_name1 = $_FILES['sliderimage']['name'][$key];
                  if($file_name != ""){
                    $file_tmp =$_FILES['sliderimage']['tmp_name'][$key];
                    /*move_uploaded_file($file_tmp,$file_name);
                    $data = file_get_contents($base_url."/".$file_name);
                    $file = file_save_data($data, "public://".$file_name, FILE_EXISTS_REPLACE);*/

                        move_uploaded_file($file_tmp,"public://digitalassetsimages/".$file_name1);
                  $data = file_get_contents("public://digitalassetsimages/".$file_name1);
                  $file_product = file_save_data($data, "public://upload_parts/".$file_name1, FILE_EXISTS_REPLACE);


                    $filew[] = array(
                        'target_id' => $file->id(),
                        'alt' => 'teee',
                        'title' => "My title"
                    );
                  }else{
                    $filew[] = array(
                        'target_id' => $target_id[$targets],
                        'alt' => 'teee',
                        'title' => "My title"
                    );
                  }
                  $targets++;
                }
                if(!empty($filew)){
                  $product2->field_image_one = $filew;
                }
              }
              $product2->save();
              $success_status = "Product updated Successfully";
            }
          }else{
            if($slider_empty_error != ""){
               $error = $slider_empty_error;
            }else if($slider_error != ""){
               $error = $slider_error;
            }else if($variation_error != ""){
               $error = $variation_error;
            }else if( ($_FILES["product_image"]["name"] != "") && ($upload_error == "") ){

              $product = Product::create([
                'type' => 'default',
                'body'  => ['value'=> $editor1,'format'=> 'basic_html'],
                'title' => t($field_product_name),
                'field_product_name' => $field_product_name,
                 'field_partno'=>$field_partno,
                 'field_category_name' => $field_category_name,
                 'field_featured'=>$field_featured,
                 'field_brand' => $field_brand,
                 'field_stock_info' => $stock,
                 'field_marketing_messages'=> $marketing_message,
                 'field_new'=>$field_new,
                 'field_sku' => 15,
                 'field_price' => 15,
                 'field_lester' => $field_lester,
                 'field_interchange' => $field_interchange,
                 'field_total_price'=> new Price($field_total_price, 'USD'),
                 'field_call_price' => $field_call_price,
                  'field_product_image' => array(
                              'target_id' => $file_product->id(),
                              'alt' => 'test',
                              'title' => "My title",
                            )
              ]);
              $product->save();
              $id =  $product->id();
              $product =Product::load($id);

              if(isset($_POST['setallcolors'])){
                $variation1   = ProductVariation::create([
                                            'type' => 'default',
                                            'sku' => $sku,
                                             'price' => new Price((string)$field_total_price, 'USD'),
                                            'field_color'=>152
                                          ]);
                $variation1->save();
                $product->addVariation($variation1);
                $vid    = 'color_parent';
                $terms  = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);

                foreach ($terms as $term) {  
                  $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
                  $field_color_price_values = $term_obj->get('field_color_price_values')->value;                  
                  $prices =  $field_total_price+$field_color_price_values;                
                  $color  =  $term->tid; 

                  if($term->tid != 152){
                    $variation   = ProductVariation::create([
                                            'type' => 'default',
                                            'sku' => $sku,
                                            'price' => new Price((string)$prices, 'USD'),
                                            'field_color'=>$color
                                          ]);
                    $variation->save();
                    $product->addVariation($variation);
                  }
                }
              }else{

                 /*default variation*/
                $variation1   = ProductVariation::create([
                                            'type' => 'default',
                                            'sku' => $sku,
                                             'price' => new Price((string)$field_total_price, 'USD'),
                                            'field_color'=>152
                                          ]);
                $variation1->save();
                $product->addVariation($variation1);
                /*end of default variation*/

                foreach($variation_array as $key=>$title_value){
                  $prices =  $field_total_price+$variation_array[$key]['price'];
                  $color  =  $variation_array[$key]['color'];
                  $variation   = ProductVariation::create([
                                            'type' => 'default',
                                            'sku' => $sku,
                                            'price' => new Price((string)$prices, 'USD'),
                                            'field_color'=>$color
                                          ]);
                  $variation->save();
                  $product->addVariation($variation);
                }
              }
              $filew = [];

              if(!empty($_FILES['sliderimage'])){
                foreach($_FILES['sliderimage']['tmp_name'] as $key => $tmp_name){
                  $file_name = $_FILES['sliderimage']['name'][$key];
                  if($file_name != ""){
                    $file_tmp =$_FILES['sliderimage']['tmp_name'][$key];
                    /*$target_file =  basename($_FILES["sliderimage"]["name"]);
                    move_uploaded_file($_FILES["sliderimage"]["tmp_name"], $target_file);*/
                   /* move_uploaded_file($file_tmp,$file_name);
                    $data = file_get_contents($base_url."/".$file_name);
                    $file = file_save_data($data, "public://".$file_name, FILE_EXISTS_REPLACE);*/

                     move_uploaded_file($file_tmp,"public://digitalassetsimages/".$file_name);
              $data = file_get_contents("public://digitalassetsimages/".$file_name);
              $file_product = file_save_data($data, "public://upload_parts/".$file_name, FILE_EXISTS_REPLACE);

                    $filew[] = array(
                          'target_id' => $file->id(),
                          'alt' => 'teee',
                          'title' => "My title"
                      );
                  }
                }
                if(!empty($filew)){
                  $product->field_image_one = $filew;
                }
              }
              $product->save();
              $success_status = "Product Added Successfully";
            }else{
              $error = "Product Image Should Be jpg or png";
            }
          }
        }
      }
      $_SESSION['postid'] = "";
    }
    if($_SESSION['postid'] == ""){
      $_SESSION['postid'] = rand(10,100);
    }
    return array(
        '#theme' => 'part_master',
        '#error'=>$error,
        '#category'=>$colors,
        '#title' => $success_status,
        '#postid'=>$_SESSION['postid']
    );
  }

  public function upload_page(){
    ini_set('memory_limit', '-1');
    ini_set('display_errors',1);
    ini_set('max_execution_time', '0');
    global $base_url;
    $msg = "";

    if(!empty($_FILES)){

      $filename = $_FILES['file']['name'];
      $excel_array=array('xls','xlsx');
      if(in_array(strtolower(pathinfo($filename, PATHINFO_EXTENSION)),$excel_array)){


        move_uploaded_file($_FILES['file']['tmp_name'],"public://bulk_template/".$filename);
        $targetPath = 'site/default/files/bulk_template/'.$filename;        

        $inputFile="public://bulk_template/".$filename;
        $extension = strtoupper(pathinfo($inputFile, PATHINFO_EXTENSION));
        $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
        $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFile);

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestDatarow();
        $highestColumn = $sheet->getHighestDataColumn();
        $title_array = $sheet->rangeToArray('A' . $row=1 . ':' . $highestColumn . $row=1, NULL, TRUE, FALSE);
          // echo "<pre>";
          // print_r($title_array);
          // die();
        $final_error_data=array();$error_count=0;$success_count=0;

        if($highestRow<=1){
          $msg="There is no data in excel";
        }

        for ($row = 1; $row <= $highestRow; $row++){
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

            if($rowData[0][0]!=''){
              if($row!=1){
              /*$rowData[0][1]="Every AutoTech Engineering unit is assembled 100% in Riverside, California. With over 100
                years of combined, “on hands” experience and the fact that we have been in business for over
                28 years has allowed us to source and supply the highest quality component parts to ensure
                your unit exceeds expectations. All are plug and play unless over wise stated. Some
                modifications such as adding spacers or plug harness are included at no extra cost. Voltage is
                set to OEM minimum specifications on each unit ordered unless otherwise requested. If you
                need higher voltage, contact us before ordering online.";*/
                $rowData[0][1] = $rowData[0][1];

              if($rowData[0][6] != ""){
                $exp_slider = explode(',', $rowData[0][6]);
                $tem_exp_slider = (count($exp_slider)>5)?array_slice($exp_slider,5):$exp_slider;

                $slider_images=explode(',', $rowData[0][6]);
                $filew=array();
                foreach ($slider_images as $key => $value) {
                     $slider_data = file_get_contents("public://digitalassetsimages/".$value);
                     $slider_product = file_save_data($slider_data, "public://upload_parts/".$value, FILE_EXISTS_REPLACE);
                     $filew[] = array(
                                'target_id' => $slider_product->id(),
                                'alt' => 'teee',
                                'title' => "My title"
                            );
                }
              }else{
                $data = file_get_contents("public://digitalassetsimages/no_image_icon.PNG");
                $slider_product = file_save_data($data, "public://upload_parts/no_image_icon.PNG", FILE_EXISTS_REPLACE);
                $filew[] = array(
                                'target_id' => $slider_product->id(),
                                'alt' => 'teee',
                                'title' => "My title"
                            );
                $rowData[0][6]="no_image_icon.PNG";

              }              


              $rowData[0][4]="Autotech";
              $rowData[0][5]="Alternators";
             
              
              }
              $error_message=$this->excel_field_validation($rowData[0],$row,$title_array[0]);


              $store_error[$row]=$error_message;
              $store_error[$row]['data'][11]=$store_error[$row]['error'];

              if($row!= 1){
                if(empty($error_message['error'])){
                     $field_partno               = $rowData[0][0];
                     $editor1                    = $rowData[0][1];
                     if($rowData[0][2] == ""){
                        $field_total_price       = 0;
                        $field_call_price        = "on";
                     }else{
                       $field_total_price       = $rowData[0][2];
                       $field_call_price        = "off";
                     }                     
                     $field_product_name         = $rowData[0][5]." | ".$rowData[0][0];


                     
                      $category['Starters']=128;
                      $category['Alternators']=129;
                      $category['C.V. Drive Axles']=146;
                      $field_category=!empty($category[$rowData[0][5]])?$category[$rowData[0][5]]:'';

                      if($rowData[0][3] != ""){
                        $data = file_get_contents("public://digitalassetsimages/".$rowData[0][3]);
                        $file_product = file_save_data($data, "public://upload_parts/".$rowData[0][3], FILE_EXISTS_REPLACE);
                      }else{
                        $data = file_get_contents("public://digitalassetsimages/no_image_icon.PNG");
                        $file_product = file_save_data($data, "public://upload_parts/no_image_icon.PNG", FILE_EXISTS_REPLACE);
                      }
                      if($rowData[0][3] == ""){
                        $alt = "No Image";
                      }else{
                        $alt = $rowData[0][3]; 
                      }
                      /*echo $field_total_price;
                      die();*/

                     $product = Product::create([
                          'type' => 'default',
                          'body'  => ['value'=> $editor1,'format'=> 'basic_html'],
                          'title' => t($field_product_name),
                          'field_product_name' => $field_product_name,
                           'field_partno'=>$field_partno,
                           'field_category_name' => $field_category,
                           'field_featured'=>1,
                           'field_brand' => 144,
                           'field_stock_info' => 1,
                           'field_marketing_messages'=> 2,
                           'field_new'=>1,
                           'field_sku' => $rowData[0][8],
                           'field_price' => 15,
                           'field_total_price'=> new Price((string)$field_total_price, 'USD'),
                           'field_product_image' => array(
                                        'target_id' => $file_product->id(),
                                        'alt' => $alt,
                                        'title' => "My title",
                                      ),
                           'field_image_one' => $filew,
                           'field_lester'=>$rowData[0][9],
                           'field_interchange'=>$rowData[0][10],
                           'field_call_price'=> $field_call_price
                      ]);
                      $product->save();
                      
                      $id =  $product->id();
                      $product_new_data =Product::load($id);
                      $vid    = 'color_parent';
                      $terms  = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
                        foreach ($terms as $term) {
                           $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
                          $field_color_price_values = $term_obj->get('field_color_price_values')->value;
                          //$color_price = ($term->tid==152)?0:15;
                          $prices      = $field_total_price+$field_color_price_values;
                          $color       = $term->tid;
                          $variation   = ProductVariation::create([
                                                    'type'  => 'default',
                                                    'sku'   => $rowData[0][8],
                                                    'price' => new Price((string)$prices , 'USD'),
                                                    'field_color'=>$color
                                                  ]);
                          $variation->save();
                          $product_new_data->addVariation($variation);
                        }
                        $product_new_data->save();                     
                        $success_count++;                        
                  }else{
                    $error_count++;
                    $final_error_data[]=$store_error[$row]['data'];
                  }
                }
            }

          }
          $res_file = "";
          if($error_count!=0){
            if($success_count<=0){
              $msg="Parts uploading failed";
            }else{
              $msg="Parts upload success with errors ";
            }
            $res_file = "sites/default/files/upload_excel_error/Errors.xls";
            $data = $this->excel_export($final_error_data,$res_file);
           
          }
        }else{
          $msg="Invalid format uploaded";
        }
      }else{
        $msg="There is no file selected";
      }

      $return_data['msg']=!empty($msg)?$msg:"Part uploaded successfully";
      $return_data['file']=!empty($res_file)?$res_file:"";
      echo json_encode($return_data);die;
    }

    public function excel_field_validation($data,$row,$title){

      $image_array=array('jpg','jpeg','png','gif');
      $msg=array();
    

      if($row==1){
        $result_data['data']=$data;
        $result_data['error']='';
      }else{
        for ($i=0; $i <11 ; $i++) {
          if($i==0 || $i==4 || $i==5){
            if(empty($data[$i])){
              $msg[$i]=$title[$i]." must not empty";
            }
            if($i==0){
                $res = $this->check_partno_exist_upload($data[$i]);
                if($res == 'exist'){
                  $msg[$i]= $title[$i]." Already Exist";
                }
              }
          }else{
              if($i==3 || $i==6){
                if($data[$i] != ""){
                  $exp_str=explode(',',$data[$i]);
                  foreach ($exp_str as $key => $value) {
                    if(!in_array(strtolower(pathinfo($value, PATHINFO_EXTENSION)),$image_array)){
                      $msg[$i]=$title[$i]." having without image";                    
                    }else{
                      if(!file_get_contents("public://digitalassetsimages/".$value)){
                        $msg[$i]=$title[$i]." having image was not existing";
                      }
                    }
                  }
                }                
              }
            }
          }
        
        $result_data['data']=$data;        
        $result_data['error']=implode(PHP_EOL,$msg);
      }
        return $result_data;
    }


  public function get_application_id($make,$model,$year,$engine){
    $result_data['engine_id']="";
    if(!empty($make) && !empty($model) && !empty($year) && !empty($engine)){
      $connection = \Drupal::database();
      $query = $connection->query("SELECT engine_id FROM catapult_make A, catapult_model B, catapult_year C, catapult_engine D where D.make_id=A.make_id and D.model_id=B.model_id and D.year_id=C.year_id and A.make='".$make."' and B.model='".$model."' and C.year='".$year."' and  D.engine='".$engine."'");
      $result=$query->fetchAssoc();
      if(!empty($result)){
        $result_data['engine_id']=$result['engine_id'];
      }
    }
    return $result_data;
  }

  public function excel_export($data,$fileName){
// print_r($data);die;
   
    global $base_url;
    $objPHPExcel = new \PHPExcel();

    $sht = $objPHPExcel->getActiveSheet();

    $i=1;
    $sht->setCellValue('A'.$i, 'Partnumber');
    $sht->setCellValue('B'.$i, 'Part Description / Key Features ');
    $sht->setCellValue('C'.$i, 'Unit Price');
    // $sht->setCellValue('D'.$i, 'Part title');
    // $sht->setCellValue('E'.$i, 'Make');
    // $sht->setCellValue('F'.$i, 'Model');
    // $sht->setCellValue('G'.$i, 'Year');
    // $sht->setCellValue('H'.$i, 'Engine');
    $sht->setCellValue('D'.$i, 'Product Image');
    $sht->setCellValue('E'.$i, 'Brand');
    $sht->setCellValue('F'.$i, 'Category');
    $sht->setCellValue('G'.$i, 'Slider Image(comma separtated)');
    $sht->setCellValue('H'.$i, 'Featured Product (Yes/No)');
    $sht->setCellValue('I'.$i, 'SKU');
    $sht->setCellValue('J'.$i, 'Lester');
    $sht->setCellValue('K'.$i, 'Interchange');
    $sht->setCellValue('L'.$i, 'Remark');

    foreach ($data as $data_key => $data_value) {
      $i++;
      $sht->setCellValue('A'.$i, $data_value[0]);
      $sht->setCellValue('B'.$i, $data_value[1]);
      $sht->setCellValue('C'.$i, $data_value[2]);
      $sht->setCellValue('D'.$i, $data_value[3]);
      $sht->setCellValue('E'.$i, $data_value[4]);
      $sht->setCellValue('F'.$i, $data_value[5]);
      $sht->setCellValue('G'.$i, $data_value[6]);
      $sht->setCellValue('H'.$i, $data_value[7]);
      $sht->setCellValue('I'.$i, $data_value[8]);
      $sht->setCellValue('J'.$i, $data_value[9]);
      $sht->setCellValue('K'.$i, $data_value[10]);
      $sht->setCellValue('L'.$i, $data_value[11]);
      // $sht->setCellValue('M'.$i, $data_value[12]);
      // $sht->setCellValue('N'.$i, $data_value[13]);
      // $sht->setCellValue('O'.$i, $data_value[14]);
      // $sht->setCellValue('P'.$i, $data_value[15]);
      // $sht->setCellValue('Q'.$i, $data_value[16]);
      $sht->getStyle('L'.$i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FF0000');
    }


    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    ob_end_clean();
    $objWriter->save($fileName);
  }


  public function getproductadmin(){
    global $base_url;
    $entity_manager     = \Drupal::entityManager();
    $all_variant = 0;
    $data1 =[];
    $connection = \Drupal::database();
    $products = array();
    $query = $connection->query("SELECT cp.product_id,fn.field_product_name_value,fp.field_partno_value,cam.field_category_name_target_id,fb.field_brand_target_id,ff.field_featured_value,ftp.field_total_price_number,fm.filename 
              FROM commerce_product cp
              LEFT JOIN commerce_product__field_product_name fn ON cp.product_id=fn.entity_id
              LEFT JOIN commerce_product__field_partno fp ON cp.product_id=fp.entity_id
              LEFT JOIN commerce_product__field_category_name cam ON cp.product_id=cam.entity_id
              LEFT JOIN commerce_product__field_brand fb ON cp.product_id=fb.entity_id
              LEFT JOIN commerce_product__field_featured ff ON cp.product_id=ff.entity_id
              LEFT JOIN `commerce_product__field_total_price` ftp ON cp.product_id=ftp.entity_id
              LEFT JOIN `commerce_product__field_product_image` fpi ON cp.product_id=fpi.entity_id
              LEFT JOIN file_managed AS fm ON fm.fid =fpi.field_product_image_target_id 
              WHERE delete_flag=0 ORDER BY product_id DESC");
    while($row = $query->fetchAssoc()){
      $id = $row['product_id'];
     /* $product      = \Drupal\commerce_product\Entity\Product::load($id);
      $vid          = 'category';
      $cid           = $row['field_category_name_target_id'];
      $term = Term::load($cid);*/
      $category_name = 'Alternators';
     /* $bid           = $row['field_brand_target_id'];
      $term = Term::load($bid);*/
      $brand_name = 'Autotech';
      $prices = number_format((float)$row['field_total_price_number'], 2, '.', '');
      $data1[]      = array('product_id'=>$row['product_id'],'productname'=>$row['field_product_name_value'],'partno'=>$row['field_partno_value'],'category_value'=>$category_name,'brand'=>$brand_name,'featured'=>$row['field_featured_value'],'price'=>$prices,'pimage'=>$base_url.'/sites/default/files/upload_parts/'.$row['filename']);
    }
    echo json_encode($data1);
    die();
  }
  public function getsingleproductadmin(){
    $all_variant              = 0;
    $id                       = $_POST['id'];
    $connection               = \Drupal::database();
    $products                 = array();
    $product                  = \Drupal\commerce_product\Entity\Product::load($id);
    $body                     =  strip_tags($product->body->value);
    $pname                    =  $product->get('field_product_name')->getValue();
    $partnos                  =  $product->get('field_partno')->getValue()[0]['value'];
    $categorys                =  $product->get('field_category_name')->getValue();
    $brands                   =  $product->get('field_brand')->getValue();
    $featured                 =  $product->get('field_featured')->getValue();
    $field_marketing_messages =  $product->get('field_marketing_messages')->getValue();
    $field_new                =  $product->get('field_new')->getValue();
    $field_stock_info         =  $product->get('field_stock_info')->getValue();
    $field_total_price        =  $product->get('field_total_price')->getValue();
    $field_new_feature        =  $product->get('field_featured')->getValue();
    $field_lester             =  $product->get('field_lester')->getValue()[0]['value'];
    $field_interchange        =  $product->get('field_interchange')->getValue()[0]['value'];
    $field_call_price     =  $product->get('field_call_price')->getValue()[0]['value'];

    //API search
    /*$keyword_or = '';
    $percentagee = '';*/
    $query = $connection->query("SELECT * FROM catapult_dive_keywords WHERE partno='".$partnos."'");
    while($row = $query->fetchAssoc()){
     $keyword_or = $row['keyword_or'];
     $keyword_and = $row['keyword_and'];
     $keyword_more = $row['exclude_keyword'];
     $percentagee = $row['percentagee'];
    }

    if($field_call_price == null){
      $field_call_price = 'off';
    }
    $pimage                   =  file_create_url($product->field_product_image->entity->getFileUri());
    $entity_manager           = \Drupal::entityManager();
    $price                    = "";
    $pricesss                 = "";
    $counts_variants =0;
    $field_marketing_messages1 = $field_marketing_messages[0]['value'];
    foreach($product->getVariationIds() as $value){
      $counts_variants = $counts_variants+1;
      $product_variation = $entity_manager->getStorage('commerce_product_variation')->load((int)$value);
      $data = $product_variation->get('price')->getValue();
      if(!empty($data) ) {
        foreach($data as $value){
          if($pricesss != ""){
            $pricesss .= ",";
          }
          $pricesss .= $value['number'];
        }
      }
    }
    if($counts_variants == 32){
      $all_variant = 1;
    }
    $productname  =  $pname[0]['value'];
    $partno       =  $partnos;
    $vid          = 'category';
    $cid          = $categorys[0]['target_id'];
    $terms        = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
      $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($cid);
      $category_name = $term->name;
    }
    $vid          = 'brand';
    $bid           = $brands[0]['target_id'];
    $terms        = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
      $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($bid);
      $brand_name = $term->name;
    }
    $data1[]      = array('product_id'=>$id,
                          'productname'=>$productname,
                          'partno'=>$partno,
                          'category_value'=>$category_name,
                          'brand'=>$brand_name,
                          'featured'=>$featured[0]['value'],
                          'pimage'=>$pimage,
                          'price'=>$pricesss,
                          'category_id'=>$cid,
                          'brand_id'=>$bid,
                          'body'=>$body,
                          'field_marketing_messages'=>$field_marketing_messages1,
                          'field_new'=>$field_new[0]['value'],
                          'field_new_feature'=>$field_new_feature[0]['value'],
                          'field_stock_info'=>$field_stock_info[0]['value'],
                          'field_total_price'=>substr($field_total_price[0]['number'], 0, -4),
                          'all_variant'=>$all_variant,
                          'field_lester'=>$field_lester,
                          'field_interchange'=>$field_interchange,
                          'field_call_price'=>$field_call_price,
                          'keyword_or'=>$keyword_or,
                          'keyword_and'=>$keyword_and,
                          'keyword_more'=>$keyword_more,
                          'percentagee'=>$percentagee
    );
    echo json_encode($data1);
    die();
  }
   public function deletefullproduct(){
    $connection  = \Drupal::database();
    $delete_product = $_POST['id'];
    $entity_manager = \Drupal::entityManager();
    $product      = \Drupal\commerce_product\Entity\Product::load($delete_product);
    $array_check  = [];
    $error = 0;
    $purchased_entity = 0;
    foreach($product->getVariationIds() as $key=>$value){
      $array_check[] = $value;
    }
   /* echo "<pre>";
    print_r($array_check);*/
    $explodes = implode(",",$array_check);

    $query = $connection->query("select count(purchased_entity) as entitys from commerce_order_item where purchased_entity
    in(".$explodes.")");


    while ($row = $query->fetchAssoc()) {

     $purchased_entity = $row['entitys'];
    }

    if($purchased_entity > 0){
      $error = 1;
    }else{
      $query = $connection->query("update commerce_product set delete_flag = 1 where product_id='".$delete_product."'");
      $error = 0;
    }
    echo $error;
    die();

  }

  public function productupdate(){
    $ids                    = $_POST['id'];
    if($_POST['values'] == 1){
      $featured = 0;
    }else{
      $featured = 1;
    }
    $productss      = \Drupal\commerce_product\Entity\Product::load($ids);

    $productss->field_featured   = $featured;
    $productss->save();
    die();
  }

  public function categoryservice(){
    $vid   = 'category';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term){
     $term_data[] = array(
      'id' => $term->tid,
      'name' => $term->name
     );
    }
    echo json_encode($term_data);
    die();
  }

   public function getcategoryid($titles){
    $vid   = 'category';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    $id = "";
    foreach ($terms as $term){
      if($term->name == $titles){
        $id =  $term->tid;
      }
    }
    echo $id;
  }

  public function brandservice(){
    $vid   = 'brand';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term){
     $term_data[] = array(
      'id' => $term->tid,
      'name' => $term->name
     );
    }
    echo json_encode($term_data);
    die();
  }

  public function getallcolors(){
    $vid   = 'color_parent';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term){
     $term_data[] = array(
      'tid' => $term->tid,
      'tname' => $term->name
     );
    }
    echo json_encode($term_data);
    die();
  }

  public function getallcolors_select($target_id,$sku,$prices,$field_total_price){
    global $base_url;
    $vid   = 'color_parent';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    $select_value = "<div class='variation_id'>
                        <div class='row form-group'>
                          <div class='col-md-3 col-lg-4 label-text'>Colors<span class='red-text'>*</span></div>
                            <div class='col-md-6 col-lg-6 colorselect'>";
    $select_value          .= "<select name='color[]' required class='form-control'>";
    foreach ($terms as $term){
      if($term->tid != 152){
        if($target_id == $term->tid){
          $select_value .= "<option selected value=".$term->tid.">".$term->name."</option>";
        }else{
          $select_value .= "<option value=".$term->tid.">".$term->name."</option>";
        }
      }      
    }
    $select_value .=  "</select></div></div>";
    $price_value   = $prices-$field_total_price;
   // die();
   /* if ((int) $price_value == $price_value) {
      $prices = $price_value;
    }else{
      $prices = substr($price_value, 0, -4);

    }
*/
     $prices = $price_value;






    $select_value .= "<div class='row form-group'>
                                    <div class='col-md-3 col-lg-4 label-text'>Price<span class='red-text'>*</span></div>
                                    <div class='col-md-6 col-lg-5'>
                                      <input required type='number' step='0.01' value=".$prices." name='price[]' class='form-control'>
                                    </div>
                                     <div class='col-md1 wd_ht'>
                                      <span id='variation_add'><img src='../themes/APA Admin Theme/images/Add-icon.png'></span>
                                      <span id='variation_remove'><img src='../themes/APA Admin Theme/images/icons/table-delete.png'></span>
                                    </div>
                                  </div>";
    $select_value .= "</div>";
    return $select_value;
    /* $select_value; */
  }

  public function getsinglevaritaion(){
    global $base_url;

    //echo $base_url;
    if( ($_POST['id'] != "") && isset($_POST['id']) ){
      $id = $_POST['id'];
      $product      = \Drupal\commerce_product\Entity\Product::load($id);
      $field_total_price      =  $product->get('field_total_price')->getValue()[0]['number'];
      $entity_manager = \Drupal::entityManager();
      $image_url =" <div class='variation_class_image'><div class='variation_imagess'>";
      foreach( ($product->get('field_image_one')->getValue()) as $key=>$value){
        $target_id = $value['target_id'];
        $file = \Drupal\file\Entity\File::load($target_id);
        $path = $file->getFileUri();
        $spliturl=$base_url."/sites/default/files/".str_replace('public://','',$path);
        $image_url .="
                                    <div class='row form-group'>
                                      <div class='col-md-3 col-lg-4 label-text'>Add Slider Image<span class='red-text'>*</span></div>
                                        <div class='col-md-6 col-lg-6'>
                                          <div class='file-upload'>
                                            <div class='file-select'>
                                              <div class='file-select-button' id='fileName'>Choose File</div>
                                              <div class='file-select-name' id='noFile'>No file chosen...</div>
                                              <input type='file' class='upl_cl' name='sliderimage[]'>
                                            </div>
                                          </div>
                                          <img style='height:50px;' src=".$spliturl.">
                                        </div>

                                        <div class='col-md1 wd_ht'>
                                          <span id='variation_add_image'><img src='../themes/APA Admin Theme/images/Add-icon.png'></span>
                                        </div>
                                    </div>";
      }
       $image_url .= " </div></div>";
      //echo file_create_url($product->field_image_one->entity->getFileUri());

      $data5 = " <div id='firstvariation'>";
      foreach($product->getVariationIds() as $key=>$value){
        $product_variation = $entity_manager->getStorage('commerce_product_variation')->load((int)$value);
        $prices = $product_variation->get('price')->getValue();
        $sku = $product_variation->get('sku')->getValue();
        $data_color = $product_variation->get('field_color')->getValue();
        $skuvalue = $sku[0]['value'];
        if($data_color[0]['target_id'] != 152){
          $data5 .= $this->getallcolors_select($data_color[0]['target_id'],$skuvalue,$prices[0]['number'],$field_total_price);
        }
      }
      $data5 .= "</div>";
      $data5 .=  "|".$image_url;
      echo $data5.'|'.$skuvalue;
      die();
    }
  }

  public function checkproducts_inorder(){
    $connection     = \Drupal::database();
    $delete_product = $_POST['id'];
    $entity_manager = \Drupal::entityManager();
    $product        = \Drupal\commerce_product\Entity\Product::load($delete_product);
    $array_check    = [];
    $error          = 0;
    $purchased_entity = 0;
    foreach($product->getVariationIds() as $key=>$value){
      $array_check[] = $value;
    }
    $explodes = implode(",",$array_check);
    if($explodes != ""){
      $query = $connection->query("select count(purchased_entity) as entitys from commerce_order_item where purchased_entity
      in(".$explodes.")");
      while ($row = $query->fetchAssoc()) {
        $purchased_entity = $row['entitys'];
      }
      if($purchased_entity > 0){
        $error = 1;
      }else{
        $error = 0;
      }
    }else{
       $error = 0;
    }
    echo $error;
    die();
  }

  public function check_partno_exist(){
    if( ($_POST['partno'] != "") && isset($_POST['partno']) ){
      $partno = $_POST['partno'];
      $data_count = 0;
      $connection = \Drupal::database();
      $query      = $connection->query("SELECT fp.entity_id  FROM commerce_product__field_partno  fp INNER JOIN  `commerce_product`  cp ON fp.entity_id=cp.product_id
        WHERE fp.field_partno_value = '".$partno."' AND cp.delete_flag='0'");
      while($row = $query->fetchAssoc()) {
        $data_count = $row['entity_id'];
      }
      if($data_count == 0){
        echo "not_exist";

      }else{
        echo "exist";
      }
    }
    die();
  }
  public function check_partno_exist_upload($partno){
    $data_count = 0;
    $connection = \Drupal::database();
    $query      = $connection->query("SELECT fp.entity_id  FROM commerce_product__field_partno  fp INNER JOIN  `commerce_product`  cp ON fp.entity_id=cp.product_id
      WHERE fp.field_partno_value = '".$partno."' AND cp.delete_flag='0'");
    while($row = $query->fetchAssoc()) {
      $data_count = $row['entity_id'];
    }
    if($data_count == 0){
      return 1;

    }else{
      return 0;
    }

  }

 /* public function upload_page_temps(){
    $error_array = [];
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', '0');
    $error = 0;
    global $base_url;
    $msg = "";
    $connection = \Drupal::database();
    $query_unit = $connection->query("select * from  unit_cost");
    $r = 0;
    while($row_unit = $query_unit->fetchAssoc()) {
      $r++;
      $field_partno       = $row_unit['Autotech_Part_No'];
      $field_total_price  = $row_unit['Unit_Cost'];
      $sku                = $row_unit['SKU'];
      $lester             = $row_unit['Lester'];

      $query_counts = $connection->query("select count(*) as counts from  commerce_product__field_partno where field_partno_value = '".$field_partno."' ");

      while($row_counts = $query_counts->fetchAssoc()) {
        $counts = $row_counts['counts'];
      }

      if($counts == 0 && $field_total_price != "" && $field_total_price != null){
          $field_product_name = "Alternators";
          $editor1            = '<p>Every AutoTech Engineering unit is assembled 100% in Riverside, California. With over 100
            years of combined, “on hands” experience and the fact that we have been in business for over
            28 years has allowed us to source and supply the highest quality component parts to ensure
            your unit exceeds expectations. All are plug and play unless over wise stated. Some
            modifications such as adding spacers or plug harness are included at no extra cost. Voltage is
            set to OEM minimum specifications on each unit ordered unless otherwise requested. If you
            need higher voltage, contact us before ordering online.</p>';

          $query_interchange = $connection->query("select im.image,im.competitorpartno from image im inner join partnumber pn on im.competitorpartno = pn.CompetitorPartno
          where pn.Autotech_Part_No = '".$field_partno."' ORDER BY RAND() ;");

          $k = 0;
          $filew = [];
          ini_set('display_errors',1);
          $filew = [];
              while ($row_interchange = $query_interchange->fetchAssoc()) {
                if($k > 5) { break; }
                  if(!file_get_contents("public://digitalassetsimages/".$row_interchange['image'])){
                       $error = 1;
                    }else{
                      if($k ==0 ){
                          $Interchange =   $row_interchange['competitorpartno'];
                          $data = file_get_contents("public://digitalassetsimages/".$row_interchange['image']);
                          $file_product = file_save_data($data, "public://upload_parts/".$row_interchange['image'], FILE_EXISTS_REPLACE);
                        }
                        $slider_data = file_get_contents("public://digitalassetsimages/".$row_interchange['image']);
                        $slider_product = file_save_data($slider_data, "public://upload_parts/".$row_interchange['image'], FILE_EXISTS_REPLACE);
                        $filew[] = array(
                                    'target_id' => $slider_product->id(),
                                    'alt' => 'teee',
                                    'title' => "My title"
                        );
                    }
                $k++;
              }
              if($error != 1 && $k > 0){

                $product = Product::create([
                  'type' => 'default',
                  'body'  => ['value'=> $editor1,'format'=> 'basic_html'],
                  'title' => t($field_product_name .' | '. $field_partno),
                  'field_product_name' => $field_product_name .' | '.$field_partno,
                   'field_partno'=>$field_partno,
                   'field_category_name' => 129,
                   'field_featured'=>1,
                   'field_brand' => 144,
                   'field_stock_info' => 1,
                   'field_marketing_messages'=> 2,
                   'field_new'=>1,
                   'field_sku' => $sku,
                   'field_price' => 15,
                   'field_total_price'=> new Price((string)$field_total_price, 'USD'),
                   'field_product_image' => array(
                                'target_id' => $file_product->id(),
                                'alt' => 'test',
                                'title' => "My title",
                              ),
                   'field_image_one' => $filew,
                   'field_lester'=>$lester,
                   'field_interchange'=>$Interchange
                ]);
                $product->save();
                $id =  $product->id();
                $product_new_data =Product::load($id);
                $vid    = 'color_parent';
                $terms  = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
                foreach ($terms as $term) {
                  $color_price = ($term->tid==152)?0:15;
                  $prices      =  $field_total_price+$color_price;
                  $color       =  $term->tid;
                  $variation   = ProductVariation::create([
                                            'type' => 'default',
                                            'sku' => $sku,
                                            'price' => new Price((string)$prices , 'USD'),
                                            'field_color'=>$color
                                          ]);

                  $variation->save();
                  $product_new_data->addVariation($variation);
                }
                $product_new_data->save();
              }else{
                $error_array[] = $field_partno;
              }
              $error = 0;
              $k = 0;

      }else{
        $missed_array[] = $field_partno;
      }
    }
    echo "<pre>";
    print_r($error_array);
    print_r($missed_array);
    die;
  }*/
  public function upload_page_temps(){



    $error_array = [];
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', '0');
    $error = 0;
    global $base_url;
    $msg="";
    $connection = \Drupal::database();

    $datas = Array
(
  'HP-14021-270',
  'HP-8248-270',
  ''
);

    /*echo "<pre>";
    print_r($datas);*/
    foreach ($datas as  $value) {
        $query_counts = $connection->query("select count(*) as counts from  commerce_product__field_partno where field_partno_value = '".$value."' ");
        while ($row_counts = $query_counts->fetchAssoc()) {
          $counts = $row_counts['counts'];
        }

        $r = 0;

        if($counts == 0){

          $query_unit = $connection->query("select * from  unit_cost where Autotech_Part_No = '".$value."' ");


          while ($row_unit = $query_unit->fetchAssoc()) {
            $field_partno       = $row_unit['Autotech_Part_No'];
            $field_total_price  = $row_unit['Unit_Cost'];
            $sku                = $row_unit['SKU'];
            $lester             = $row_unit['Lester'];
            $field_product_name = "Alternators";

            $editor1            = '<p>Every AutoTech Engineering unit is assembled 100% in Riverside, California. With over 100
              years of combined, “on hands” experience and the fact that we have been in business for over
              28 years has allowed us to source and supply the highest quality component parts to ensure
              your unit exceeds expectations. All are plug and play unless over wise stated. Some
              modifications such as adding spacers or plug harness are included at no extra cost. Voltage is
              set to OEM minimum specifications on each unit ordered unless otherwise requested. If you
              need higher voltage, contact us before ordering online.</p>';


            /*$query_interchange = $connection->query("select im.image,im.competitorpartno from image im inner join partnumber pn on im.competitorpartno = pn.CompetitorPartno
          where pn.Autotech_Part_No = '".$field_partno."' ORDER BY RAND() ;");*/

          $query_interchange = $connection->query("select CompetitorPartno from partnumber as pn where Autotech_Part_No = '".$field_partno."' ORDER BY RAND() ;");




            $k = 0;
            $filew = [];
            ini_set('display_errors',1);
            $filew = [];
            $file_product = '';
            while ($row_interchange = $query_interchange->fetchAssoc()) {
              /*echo $row_interchange['CompetitorPartno'];
              die();*/

                if($k > 5) break;
                if(!file_get_contents("public://digitalassetsimages/no_image_icon.PNG")){
                     $error = 1;
                }else{
                  if($k == 0 ){
                    $Interchange =   $row_interchange['CompetitorPartno'];
                    $data = file_get_contents("public://digitalassetsimages/no_image_icon.PNG");
                    $file_product = file_save_data($data, "public://upload_parts/no_image_icon.PNG", FILE_EXISTS_REPLACE);
                  }
                  $slider_data = file_get_contents("public://digitalassetsimages/no_image_icon.PNG");
                  $slider_product = file_save_data($slider_data, "public://upload_parts/no_image_icon.PNG", FILE_EXISTS_REPLACE);
                  $filew[] = array(
                              'target_id' => $slider_product->id(),
                              'alt' => 'teee',
                              'title' => "My title"
                  );

                }

              $k++;
            }


            if($error != 1 && $k > 0 && $file_product != ''){

                $product = Product::create([
                  'type' => 'default',
                  'body'  => ['value'=> $editor1,'format'=> 'basic_html'],
                  'title' => t($field_product_name .' | '. $field_partno),
                  'field_product_name' => $field_product_name .' | '.$field_partno,
                   'field_partno'=>$field_partno,
                   'field_category_name' => 129,
                   'field_featured'=>1,
                   'field_brand' => 144,
                   'field_stock_info' => 1,
                   'field_marketing_messages'=> 2,
                   'field_new'=>1,
                   'field_sku' => $sku,
                   'field_price' => 15,
                   'field_total_price'=> new Price((string)$field_total_price, 'USD'),
                   'field_product_image' => array(
                                'target_id' => $file_product->id(),
                                'alt' => 'test',
                                'title' => "My title",
                              ),
                   'field_image_one' => $filew,
                   'field_lester'=>$lester,
                   'field_interchange'=>$Interchange
                ]);
                $product->save();
                $id =  $product->id();
                $product_new_data =Product::load($id);
                $vid    = 'color_parent';
                $terms  = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
                foreach ($terms as $term) {
                  $color_price=($term->tid==152)?0:15;
                  $prices =  $field_total_price+$color_price;
                  $color  =  $term->tid;
                  $variation   = ProductVariation::create([
                                            'type' => 'default',
                                            'sku' => $sku,
                                            'price' => new Price((string)$prices , 'USD'),
                                            'field_color'=>$color
                                          ]);

                  $variation->save();
                  $product_new_data->addVariation($variation);
                }
              $product_new_data->save();

            }else{
              $error_array[] = $field_partno;
            }
            $error = 0;
            $k = 0;
            break;

          }

        }else{

        }
    }
    echo "<pre>";
    print_r($error_array);


    die;
  }

 /* public function parnto_excel_download(){
    $connection = \Drupal::database();
    $query_counts = $connection->query("SELECT field_partno_value,entity_id FROM commerce_product__field_partno");
    $entity_manager = \Drupal::entityManager();
    $r= 0;
    $array_data = [];
    while ($row_counts = $query_counts->fetchAssoc()) {
      $field_partno_value = $row_counts['field_partno_value'];
      $entity_id = $row_counts['entity_id'];
      $product   = \Drupal\commerce_product\Entity\Product::load($entity_id);
      foreach( ($product->get('field_image_one')->getValue()) as $key=>$value){
        $target_id = $value['target_id'];
        $file = \Drupal\file\Entity\File::load($target_id);
        $path = $file->getFilename();
        $array_data[] = array('filename'=>$path,'partno'=>$field_partno_value);
      }
      $r++;
    }
    $file = fopen("image_check.csv","w");
    foreach ($array_data as $line) {
      fputcsv($file, $line);
    }
    die();
  }*/

  public function parnto_excel_download(){
    $connection = \Drupal::database();
    $query_counts = $connection->query("SELECT field_partno_value,entity_id FROM commerce_product__field_partno limit 1401,500");
    $entity_manager = \Drupal::entityManager();
    $r              = 0;
    $array_data     = [];
    $r =1;
    $array_data[] = array('parnto'=>'Partno',
                            'price'=>'Price',
                            'lester'=>'Lester',
                            'sku'=>'SKU',
                            'image'=>'Image Available'
                           );
    while ($row_counts = $query_counts->fetchAssoc()) {
      $field_partno_value       = $row_counts['field_partno_value'];
      $entity_id                = $row_counts['entity_id'];
      $product                  = \Drupal\commerce_product\Entity\Product::load($entity_id);
      $field_total_price        = $product->get('field_total_price')->getValue()[0]['number'];
      $field_lester             = $product->get('field_lester')->getValue()[0]['value'];
      $field_interchange        = $product->get('field_interchange')->getValue()[0]['value'];
      $image                    = "";
      $pimage                   =  file_create_url($product->field_product_image->entity->getFileUri());
      if($pimage == "http://localhost/autotech1/sites/default/files/upload_parts/no_image_icon.PNG"){
        $pimage = "Image Not Available";
      }else{
        $pimage = "Image Available";
      }
      /*foreach( ($product->get('field_image_one')->getValue()) as $key=>$value){
        $target_id = $value['target_id'];
        $file = \Drupal\file\Entity\File::load($target_id);
        $path  = $file->getFilename();
        if($path=='no_image_icon.PNG'){
          $image = "No";
          break;
        }else{
          $image = "Yes";
          break;
        }
      }  */
      foreach($product->getVariationIds() as $value){
        $product_variation = $entity_manager->getStorage('commerce_product_variation')->load((int)$value);
        $sku               = $product_variation->get('sku')->getValue()[0]['value'];
      }
      $array_data[] = array('parnto'=>$field_partno_value,
                            'price'=>$field_total_price,
                            'lester'=>$field_lester,
                            'sku'=>$sku,
                            'image'=>$pimage
                           );

      $r++; 
       
    }    

    $file = fopen("part_detail_work_1800.csv","w");
    foreach ($array_data as $line) {
      fputcsv($file, $line);
    }
    die();
  }

  public function API_Search_Result(){
      $url = $_POST['url'];
      $connection = curl_init();
      curl_setopt($connection, CURLOPT_URL, $url);
      curl_setopt($connection, CURLOPT_POST, 0);
      curl_setopt($connection, CURLOPT_HEADER, 0);
      curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);
      $response = curl_exec($connection);
      $xml = simplexml_load_string($response);

      $result = [];
      foreach ($xml->searchResult->item as $value) {
          $title = (string)$value->title;
          $title = preg_replace('/\\\\/', '', $title);
          $title = str_replace("'", "\'" ,$title);
          $title = str_replace('"', '\"' ,$title);

          $currentPrice = (string)$value->sellingStatus->currentPrice;
          $shippingPrice = (string)$value->shippingInfo->shippingServiceCost;
          if ($shippingPrice == ''){
              $shippingPrice = 0;
          }
          $totalPrice = $currentPrice + $shippingPrice;

          $result[] = array('itemid'  => (string)$value->itemId,
                          'title'   => $title,
                          'galleryURL'  => (string)$value->galleryURL,
                          'sellerUserName'  => (string)$value->sellerInfo->sellerUserName,
                          'currentPrice'  => $currentPrice,
                          'shippingPrice'  => $shippingPrice,
                          'totalPrice'  => round($totalPrice,2)
                    );
      }
      
      echo json_encode($result);
      die();
  }

  public function CLP_keyword_save(){
  
    $partno          = $_POST['partno'];
    $or_keyword      = $_POST['or_keyword'];
    $and_keyword     = $_POST['and_keyword'];
    $exclude_keyword = $_POST['exclude_keyword'];
    $percentage      = $_POST['percentage'];
    $connection = \Drupal::database();
    $part_exist = $connection->query("SELECT 1 FROM catapult_dive_keywords where partno='".$partno."'");
    $exist_count = $part_exist->fetchAssoc();

    if($exist_count[1] >= 1){
        $part_res = $connection->query("UPDATE catapult_dive_keywords SET keyword_or='".$or_keyword."',keyword_and='".$and_keyword."', exclude_keyword='".$exclude_keyword."', percentagee='".$percentage."' WHERE partno ='".$partno."'");
    }else{
        $part_res = $connection->query("INSERT INTO catapult_dive_keywords (partno, keyword_or,keyword_and,exclude_keyword, percentagee, created_by,created_on) VALUES('".$partno."','".$or_keyword."','".$and_keyword."','".$exclude_keyword."','".$percentage."','site_admin',NOW())");
    }
   
    echo json_encode(array('res'=>'success'));
    die();
  }
}
