<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;
use Symfony\Component\HttpFoundation\JsonResponse;
class Homepage_news{
	public function __construct(){
		$this->connection = \Drupal::database();		
	}
  public function page(){
  		$nidss = \Drupal::entityQuery('node')->condition('type','homepage_category_details')->execute();

        foreach($nidss as $key => $idss){
			$node = \Drupal\node\Entity\Node::load($idss);						
			$cat_array[] = array(   'title'=>$node->getTitle(),							        					        
							        'category_name'=>strip_tags($node->field_category_name_data->value),
							        'id'=>$idss							        
	        					);
	        						}
        
	global $base_url;  	
  	$success_status = "";
  	$upload_error = "";
  	$error = "";
  	$editor_validate = "";
  	if(isset($_SESSION['postid']) ){
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);
      }
    }else{
      $_SESSION['postid'] = rand(10,100);
    }	
  	if(!empty($_POST)){
      	if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
	        if($_SESSION['postid'] == $_POST['postid']){
		  		$value    = $_POST['editor1'];
		  		$detailval    = $_POST['editor2'];
		  		$sequence    = $_POST['sequence'];
		  		$category_name    = $_POST['category_name'];
		  		$news_date    = date('Y-m-d',strtotime($_POST['news_date']));
		  	//	print_r($_POST['news_date']);die;
		  		if(($value == "" || $value == "<p><br></p>")|| ($detailval == "" || $detailval == "<p><br></p>")){
		  			$editor_validate = "Please Enter Content,Detail Content";
		  		}else{
					$title    = $_POST['head'];
					if($_POST['hidden_id'] != ""){
						/*echo "up";
						exit;*/
						$node                         = Node::load($_POST['hidden_id']);	
			  			$node->body->value            = $value;
						$node->body->format           = 'full_html';
						$node->field_homepage_news_data      = $detailval;
						$node->field_homepage_news_data->format   = 'full_html';
						$node->title                  = $title;
						$node->field_sequence_news->value = $sequence;
						$node->field_news_cat->value = $category_name;
						$node->field_news_date->value = $news_date;
						$node->save();
						$success_status = "News Updated Successfully";
						
			  		}else{
			  			/*echo "down";
						exit;*/
			  			$node = Node::create([
									'type'  => 'homepage_news',
									'title'	=> $title,
									'field_news_cat' => $category_name,
									'field_news_date' => $news_date,										
									'field_homepage_news_data' =>['value'=> $detailval,'format'=> 'full_html'],
									'body'	=> ['value'=> $value,'format'=> 'full_html'],
									'field_sequence_news' => $sequence									
								]);
						$node->save();
						$success_status = "News Added Successfully";
			  			
					}
				}	
			}
		}	
		$_SESSION['postid'] = "";	
  	} 
  	if($_SESSION['postid'] == ""){
		$_SESSION['postid'] = rand(10,100);      
	} 
	if($upload_error != ""){
		$error = $upload_error;
	}
	if($editor_validate != ""){
		$error = $editor_validate;
	}
    return array('#theme' => 'homepage_news_setting',
    			 '#title' => $success_status,
    			 '#postid'=>$_SESSION['postid'],
    			 '#error'=> $error,
    			 '#items'=>$cat_array
				);
  }

  	public function homepage_news_url(){  		
		$service_array =[];
	  	$nids = \Drupal::entityQuery('node')->condition('type','homepage_news')->execute();
	  		
	  	foreach($nids as $key => $ids){
			$node = \Drupal\node\Entity\Node::load($ids);
			$res = $node->field_sequence_news->getValue();	
			$news_cat_name=$this->category_details($node->field_news_cat->value);
			 // print_r($news_cat);die;			
			$service_array[] = array(
							        'title'=>$node->getTitle(),
							        'body'=>strip_tags($node->body->value),	
							        'news_cat'=>$news_cat_name,
					                'news_date'=>date('d-m-Y',strtotime($node->field_news_date->value)),					        
							        'detailval'=>strip_tags($node->field_homepage_news_data->value),
							        'id'=>$ids,
							        'sequence'=>$res[0]['value']
	        						);
		}		
		return new JsonResponse([
	      $service_array
	    ]);
  	}

  	public function category_details($id){
  		//print_r($id);die;

	   $query = $this->connection->query("SELECT * FROM node__field_category_name_data where entity_id='".$id."' ");     
       $row = $query->fetchAssoc();
       $cat_name=$row['field_category_name_data_value'];
       return $cat_name;

  	}

  	public function homepage_news_delete(){
     
	    $nodeid = $_POST['id'];
	    $res = array($nodeid=>$nodeid);
		entity_delete_multiple('node', $res);
		die();
  }
  public function homepage_news_edit(){      
  	$id   = $_POST['id'];
  	$node = \Drupal\node\Entity\Node::load($id);

	$res = $node->field_sequence_news->getValue();	
    //$node1 = \Drupal\node\Entity\Node::load($node->field_news_cat->value);
    //echo "<pre>";//print_r($node1);die;
	$service_array[] = array(
					        'title'=>$node->getTitle(),
					        'body'=>$node->body->value,
					        'news_cat'=>$node->field_news_cat->value,
					        'news_date'=>date('d-m-Y',strtotime($node->field_news_date->value)),
					        'id'=>$id,
					        'detailval'=>$node->get('field_homepage_news_data')->value,
					       	'sequence'=>$res[0]['value']
    						);
	echo json_encode($service_array);
	exit();
  }  
}