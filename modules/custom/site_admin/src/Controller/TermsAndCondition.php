<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;

class TermsAndCondition{

  public function page(){
  	$connection = \Drupal::database();
    $success_status = "";
    if(isset($_SESSION['postid']) ){
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);
      }
    }else{
      $_SESSION['postid'] = rand(10,100);
    }   
    $success_status  = "";
    $upload_error    = "";
    $error           = "";
    $editor_validate = "";    
    if(!empty($_POST)){
      if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
        if($_SESSION['postid'] == $_POST['postid']){

          $heading         = $_POST['heading'];
          $editor1         = $_POST['editor1'];
          $sequence        = $_POST['sequence'];
          if($editor1 == ""){
            $editor_validate = "Please Enter Content";
          }else{     
            if($_POST['hidden_id'] != ""){
              $node                         = Node::load($_POST['hidden_id']);      
              $node->body->value            = $editor1;
              $node->body->format           = 'full_html';
              $node->title                  = $heading;
              $node->field_terms_sequence->value = $sequence;     
              $node->save();
              $success_status = "Terms And conditions Updated Successfully";
            }else{        
                $body = [
                'value' => $editor1,
                'format' => 'basic_html',
                ];
                
              $node = Node::create([
                'type'  => 'terms_and_conditions',
                'title' => $heading,
                'body'  => ['value'=> $editor1,'format'=> 'basic_html'],
                'field_terms_sequence' => $sequence,
              ]);
              $node->save();
              $success_status = "Terms And conditions Added Successfully";
            }
          }     
        }
      }
      $_SESSION['postid'] = "";
  	}
    if($editor_validate != ""){
      $error = $editor_validate;
    }
    if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);      
    }
    return array(
        '#theme' => 'terms_condition',
        '#title' => $success_status,
        '#postid'=>$_SESSION['postid'],
        '#error'=>$error
    );
  }

  public function termsurl(){     
    $terms_array =[];
      $nids          = \Drupal::entityQuery('node')->condition('type','terms_and_conditions')->execute();
      foreach($nids as $key => $ids){
      $node = \Drupal\node\Entity\Node::load($ids);
      $res = $node->field_terms_sequence->getValue();   
      $terms_array[] = array(
                      'title'=>$node->getTitle(),
                      'body'=>strip_tags($node->body->value),
                      'sequence'=>$res[0]['value'],
                      'id'=>$ids
                      );
    }
    echo json_encode($terms_array); 
    exit(); 
  }

  public function termsdeletenode(){ 
    $nodeid = $_POST['id'];
    $typess = $_POST['typess'];
    if($typess == 'terms_and_conditions'){
      $result = \Drupal::entityQuery('node')
        ->condition('type', 'terms_and_conditions')
        ->execute();  
    }     
    $res = array($nodeid=>$nodeid);
    entity_delete_multiple('node', $res);
    die();
  }

  public function termssinglenode(){ 
    $id   = $_POST['id'];
    $node = \Drupal\node\Entity\Node::load($id);
    $res  = $node->field_terms_sequence->getValue();    
    $terms_array[] = array(
                    'title'=>$node->getTitle(),
                    'body'=>strip_tags($node->body->value),
                    'sequence'=>$res[0]['value'],
                    'id'=>$id
                  );
    echo json_encode($terms_array);
    exit();
  }
}
