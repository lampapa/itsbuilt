<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;

//for catlisting in header
//use Drupal\drupalup_controller\Controller;

class Faq{

  public function page() {
    $connection = \Drupal::database();
    $success_status = "";
    if(isset($_SESSION['postid']) ){
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);
      }
    }else{
      $_SESSION['postid'] = rand(10,100);
    }   
    $success_status = "";
    $upload_error = "";
    $error = "";
    $editor_validate = "";
    if(!empty($_POST)){
      if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
        if($_SESSION['postid'] == $_POST['postid']){               
          $faqheading     = $_POST['faqheading'];
          $faq_question   = $_POST['faq_question'];
          $faq_answer     = $_POST['faq_answer'];
          $faq_sequence   = $_POST['faq_sequence'];                
          if($_POST['hidden_id'] != ""){
            $node                         = Node::load($_POST['hidden_id']);      
            $node->body->value            = $faq_question;
            $node->body->format           = 'full_html';
            $node->title                  = $faqheading;
            $node->field_sequence->value  = $faq_sequence;  
            $node->field_answer->value    = $faq_answer;    
            $node->save();
            $success_status = "Faq Updated Successfully";
          }else{            
            $body = [
            'value' => $faq_question,
            'format' => 'basic_html',
            ];                
            $node = Node::create([
              'type'  => 'faq',
              'title' => $faqheading,
              'body'  => ['value'=> $faq_question,'format'=> 'basic_html'],
              'field_sequence' => $faq_sequence,
              'field_answer' => $faq_answer
            ]);
            $node->save();            
            $success_status = "Faq Added Successfully";
          }
        }  
      }
      $_SESSION['postid'] = "";                  
    }
    if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);      
    }  
    return array('#theme' => 'faq',
             '#title' => $success_status,
             '#postid'=>$_SESSION['postid'],
             '#error'=>$error
            );
  }

   public function faqurl(){     
    $terms_array =[];
    $nids          = \Drupal::entityQuery('node')->condition('type','faq')->execute();
    foreach($nids as $key => $ids){
      $node = \Drupal\node\Entity\Node::load($ids);
      $res = $node->field_sequence->getValue();  
      $res1 = $node->field_answer->getValue();  
      $terms_array[] = array(
                      'title'=>$node->getTitle(),
                      'body'=>strip_tags($node->body->value),
                      'faq_sequence'=>$res[0]['value'],
                      'faq_answer'=>$res1[0]['value'],
                      'id'=>$ids
                      );
    }
    echo json_encode($terms_array); 
    exit(); 
  }

  public function faqdeletenode(){ 
    $nodeid = $_POST['id'];
    $typess = $_POST['typess'];
    if($typess == 'faq'){
      $result = \Drupal::entityQuery('node')
          ->condition('type', 'faq')
          ->execute();  
    }else if($typess == 'aboutus'){
      $result = \Drupal::entityQuery('node')
          ->condition('type', 'about_us')
          ->execute();  
    }      
      $res = array($nodeid=>$nodeid);
      entity_delete_multiple('node', $res);
      die();
  }

  public function faqsinglenode(){ 
    $id   = $_POST['id'];
    $node = \Drupal\node\Entity\Node::load($id);
    $res = $node->field_sequence->getValue();  
    $res1 = $node->field_answer->getValue();    
    $terms_array[] = array(
        'title'=>$node->getTitle(),
        'body'=>strip_tags($node->body->value),
        'faq_sequence'=>$res[0]['value'],
        'faq_answer'=>$res1[0]['value'],
        'id'=>$id
      );
    echo json_encode($terms_array);
    exit();
  }
  public function addfaqtopics(){
    $faqtopics      = $_POST['faqtopics'];
    $connection  = \Drupal::database();         
    $query       = $connection->query("insert into catapult_faq_categories(faq_categoryname,created_by,created_on,delete_status) values('".$faqtopics."','JP',Now(),'N')");        
      exit();       
  }
  public function deletefaqtopics(){
    $id          = $_POST['ids'];
    $connection  = \Drupal::database();
    $query       = $connection->query("delete from catapult_faq_categories where faq_cat_id='".$id."'");
      exit();
  }
  public function getfaq(){
    $connection  = \Drupal::database();
        $user        = \Drupal::currentUser();
        $user_id     = $user->id();
      $query       = $connection->query("SELECT * FROM catapult_faq_categories");
      $products =[];
        while($row = $query->fetchAssoc()){           
          $product1 = array("id"=>$row['faq_cat_id'],"faq_category"=>$row['faq_categoryname']);
          $products[] = $product1;
        }
        echo json_encode($products);
        exit();       
  }
}