<?php
namespace Drupal\site_admin\Controller;
class SalesDashboard{
  public function page() {

    return array(
      '#theme' => 'sales_dashboard'     
    );
  }

  public function salesurl(){
    $intervl = $_POST['intervl'];
    $connection  = \Drupal::database();

    if($intervl == 1){
        $query1 = $connection->query("SELECT IFNULL(ROUND(SUM(total_price__number),2),0)  AS totalsales, 
        COUNT(order_id)  AS neworders, 
        (SELECT COUNT(order_id)  FROM  commerce_order WHERE cart=1 AND state='draft' 
        AND DATE_FORMAT(FROM_UNIXTIME(created), '%Y-%m-%d')  >= (DATE_FORMAT(NOW(), '%Y-%m-%d') )) AS potentialorder, 0 AS cancelledorder
        FROM commerce_order 
        WHERE cart=0 AND state='completed'
       AND DATE_FORMAT(FROM_UNIXTIME(created), '%Y-%m-%d')  >= (DATE_FORMAT(NOW(), '%Y-%m-%d') )");
        $row1 = $query1->fetchAll();
        

        $query2 = $connection->query(" SELECT (DATE_FORMAT(FROM_UNIXTIME(c.created), '%Y-%m-%d %H:%i:%s')) AS created ,ROUND(COUNT(i.quantity)) AS quantity 
            FROM `commerce_order` c  JOIN `commerce_order_item` i
            ON c.order_id = i.order_id
            WHERE c.cart=0 AND c.state='completed' 
            AND DATE_FORMAT(FROM_UNIXTIME(c.created), '%Y-%m-%d')  = (DATE_FORMAT(NOW(), '%Y-%m-%d'))
            GROUP  BY (DATE_FORMAT(FROM_UNIXTIME(c.created), '%Y-%m-%d %H:%i:%s'))
            ORDER BY (DATE_FORMAT(FROM_UNIXTIME(c.created), '%Y-%m-%d %H:%i:%s'))");
        $row2 = $query2->fetchAll();

        $query3 = $connection->query("SELECT DATE_FORMAT(FROM_UNIXTIME(c.created), '%Y-%m') AS month ,ROUND(SUM(c.total_price__number),2) AS Sales
          FROM `commerce_order` c
          WHERE c.cart=0 AND c.state='completed' 
          GROUP  BY DATE_FORMAT(FROM_UNIXTIME(c.created), '%Y-%m')");
        $row3 = $query3->fetchAll();

        $query4 = $connection->query("SELECT c.order_id,
              (SELECT NAME FROM users_field_data WHERE uid=c.uid) AS customername,
              i.title,ROUND(i.quantity) AS quantity ,
              DATE_FORMAT(FROM_UNIXTIME(i.created), '%d/%m/%Y') AS purchased_on,
              'placed' AS statuss
              FROM commerce_order c JOIN commerce_order_item i
              ON c.order_id = i.order_id
              WHERE c.cart=0 AND c.state='completed'
              AND DATE_FORMAT(FROM_UNIXTIME(i.created), '%Y-%m-%d')  >= (DATE_FORMAT(NOW(), '%Y-%m-%d') )");
        $row4 = $query4->fetchAll();

        $data = array('first'=>$row1,'orderdetails'=>$row2,'salesdetails'=>$row3,'orderstatus'=>$row4);
        echo json_encode($data);
        die();

    }else{
      $query1 = $connection->query("SELECT IFNULL(ROUND(SUM(total_price__number),2),0)  AS totalsales, 
        COUNT(order_id)  AS neworders, 
        (SELECT COUNT(order_id)  FROM  commerce_order WHERE cart=1 AND state='draft' 
        AND DATE_FORMAT(FROM_UNIXTIME(created), '%Y-%m-%d')  >= (DATE_FORMAT(NOW(), '%Y-%m-%d')  - INTERVAL ".$intervl." DAY)) AS potentialorder, 0 AS cancelledorder
        FROM commerce_order 
        WHERE cart=0 AND state='completed'
        AND DATE_FORMAT(FROM_UNIXTIME(created), '%Y-%m-%d')  >= (DATE_FORMAT(NOW(), '%Y-%m-%d')  - INTERVAL ".$intervl." DAY)");
      $row1 = $query1->fetchAll();
      

      $query2 = $connection->query("SELECT DATE_FORMAT(FROM_UNIXTIME(c.created), '%Y-%m-%d') AS created ,ROUND(COUNT(i.quantity)) AS quantity 
        FROM `commerce_order` c  JOIN `commerce_order_item` i
        ON c.order_id = i.order_id
        WHERE c.cart=0 AND c.state='completed' 
        AND DATE_FORMAT(FROM_UNIXTIME(c.created), '%Y-%m-%d')  >= (DATE_FORMAT(NOW(), '%Y-%m-%d')  - INTERVAL ".$intervl." DAY)
        GROUP  BY DATE_FORMAT(FROM_UNIXTIME(c.created), '%Y-%m-%d')");
      $row2 = $query2->fetchAll();

      $query3 = $connection->query("SELECT DATE_FORMAT(FROM_UNIXTIME(c.created), '%Y-%m') AS month ,ROUND(SUM(c.total_price__number),2) AS Sales
        FROM `commerce_order` c 
        WHERE c.cart=0 AND c.state='completed' 
        GROUP  BY DATE_FORMAT(FROM_UNIXTIME(c.created), '%Y-%m')");
      $row3 = $query3->fetchAll();

      $query4 = $connection->query("SELECT c.order_id,
                (SELECT NAME FROM users_field_data WHERE uid=c.uid) AS customername,
                i.title,ROUND(i.quantity) AS quantity ,
                DATE_FORMAT(FROM_UNIXTIME(i.created), '%d/%m/%Y') AS purchased_on,
                'placed' AS statuss
                FROM commerce_order c JOIN commerce_order_item i
                ON c.order_id = i.order_id
                WHERE c.cart=0 AND c.state='completed'
                AND DATE_FORMAT(FROM_UNIXTIME(i.created), '%Y-%m-%d')  >= (DATE_FORMAT(NOW(), '%Y-%m-%d')  - INTERVAL ".$intervl." DAY)");
      $row4 = $query4->fetchAll();

      $data = array('first'=>$row1,'orderdetails'=>$row2,'salesdetails'=>$row3,'orderstatus'=>$row4);
      echo json_encode($data);
      die();
    }
  }

  public function dashboard_datepicker1(){
    $from = $_POST['from'];
    $to = $_POST['to'];
    $connection  = \Drupal::database();
    $query1 = $connection->query("SELECT IFNULL(ROUND(SUM(total_price__number),2),0)  AS totalsales, 
        COUNT(order_id)  AS neworders, 
        (SELECT COUNT(order_id)  FROM  commerce_order WHERE cart=1 AND state='draft' 
        AND  DATE_FORMAT(FROM_UNIXTIME(created), '%Y-%m-%d')  BETWEEN '".$from."'  AND '".$to."') AS potentialorder, 0 AS cancelledorder
        FROM commerce_order 
        WHERE cart=0 AND state='completed'
        AND  DATE_FORMAT(FROM_UNIXTIME(created), '%Y-%m-%d')  BETWEEN '".$from."'  AND '".$to."'");
    $row1 = $query1->fetchAll();
   

   $query2 = $connection->query("SELECT DATE_FORMAT(FROM_UNIXTIME(c.created), '%Y-%m-%d') AS created ,ROUND(COUNT(i.quantity)) AS quantity 
      FROM `commerce_order` c  JOIN `commerce_order_item` i
      ON c.order_id = i.order_id
      WHERE c.cart=0 AND c.state='completed' 
      AND  DATE_FORMAT(FROM_UNIXTIME(c.created), '%Y-%m-%d')  BETWEEN '".$from."'  AND '".$to."'
      GROUP  BY DATE_FORMAT(FROM_UNIXTIME(c.created), '%Y-%m-%d')");
    $row2 = $query2->fetchAll();
    
    $query4 = $connection->query("SELECT c.order_id,
              (SELECT NAME FROM users_field_data WHERE uid=c.uid) AS customername,
              i.title,ROUND(i.quantity) AS quantity ,
              DATE_FORMAT(FROM_UNIXTIME(i.created), '%d/%m/%Y') AS purchased_on,
              'placed' AS statuss
              FROM commerce_order c JOIN commerce_order_item i
              ON c.order_id = i.order_id
              WHERE c.cart=0 AND c.state='completed'
              AND  DATE_FORMAT(FROM_UNIXTIME(i.created), '%Y-%m-%d')  BETWEEN '".$from."'  AND '".$to."'");
    $row4 = $query4->fetchAll();
   
    $data = array('first'=>$row1,'orderdetails'=>$row2,'orderstatus'=>$row4);
    echo json_encode($data);
    die();
  }

}