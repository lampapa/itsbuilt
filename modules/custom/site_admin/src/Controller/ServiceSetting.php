<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;

class ServiceSetting{
  public function page(){
  	global $base_url;  	
  	$success_status = "";
  	$upload_error = "";
  	$error = "";
  	$editor_validate = "";
 //  	if($_SESSION['postid'] == ""){
 //  		$_SESSION['postid'] = rand(10,100);      
	// }
	if(isset($_SESSION['postid']) ){
		if($_SESSION['postid'] == ""){
			$_SESSION['postid'] = rand(10,100);
		}
	}else{
		$_SESSION['postid'] = rand(10,100);
	}
  	if(!empty($_POST)){
      	if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
	        if($_SESSION['postid'] == $_POST['postid']){
		  		$value    = $_POST['editor1'];
		  		if($value == ""){
		  			$editor_validate = "Please Enter Content";
		  		}else{
					$title    = $_POST['head'];
					if($_POST['hidden_id'] != ""){

			  			$node                         = Node::load($_POST['hidden_id']);	
			  			$node->body->value            = $value;
						$node->body->format           = 'full_html';
						$node->title                  = $title;
						$node->save();
						$success_status = "Service Updated Successfully";
						
			  		}else{
			  			/*echo "down";
			  			exit;*/
			  			$node = Node::create([
									'type'  => 'services',
									'title'	=> $title,
									'body'	=> ['value'=> $value,'format'=> 'basic_html']
									
								]);
						$node->save();
						
						$success_status = "Service Added Successfully";
			  			
					}
				}	
			}
		}	
		$_SESSION['postid'] = "";	
  	} 
  	if($_SESSION['postid'] == ""){
		$_SESSION['postid'] = rand(10,100);      
	} 
	if($upload_error != ""){
		$error = $upload_error;
	}
	if($editor_validate != ""){
		$error = $editor_validate;
	}
    return array('#theme' => 'service_setting',
    			 '#title' => $success_status,
    			 '#postid'=>$_SESSION['postid'],
    			 '#error'=> $error
				);
  }

  	public function serviceurl(){  		
		$service_array =[];
	  	$nids          = \Drupal::entityQuery('node')->condition('type','services')->execute();
	  	foreach($nids as $key => $ids){
			$node = \Drupal\node\Entity\Node::load($ids);
			/*$res = $node->field_service_sequence->getValue();
			if(empty($res)){
				$res = "";
			}else{
				$res = $res[0]['value'];	
			}	*/
			$service_array[] = array(
							        'title'=>$node->getTitle(),
							        'body'=>$node->body->value,
							        /*'file'=>file_create_url($node->field_service_image->entity->getFileUri()),
							        'sequence'=>$res,*/
							        'id'=>$ids
	        						);
		}
		echo json_encode($service_array);	
		exit();	
  	}

  	public function deletenode(){ 
	  	/*$nodeid = $_POST['id'];
	  	$typess = $_POST['typess'];
	  	if($typess == 'services'){
		  	$result = \Drupal::entityQuery('node')
		    ->condition('type', 'services')
		    ->execute();	
		}else if($typess == 'aboutus'){
			$result = \Drupal::entityQuery('node')
		    ->condition('type', 'about_us')
		    ->execute();	
		}else if($typess == 'products'){
			$delete_product = $_POST['id'];
		    $res = array($nodeid=>$delete_product);
		    $storage_handler = \Drupal::entityTypeManager()->getStorage("commerce_product");
		    $entities = $storage_handler->loadMultiple($res);
		    $storage_handler->delete($entities);
		}      
	  	$res = array($nodeid=>$nodeid);
		entity_delete_multiple('node', $res);
		die();*/
  	}
    public function singlenode(){ 
	  	/*$id   = $_POST['id'];
	  	$node = \Drupal\node\Entity\Node::load($id);
		$res  = $node->field_service_sequence->getValue();		
		$service_array[] = array(
						        'title'=>$node->getTitle(),
						        'body'=>strip_tags($node->body->value),
						        'file'=>file_create_url($node->field_service_image->entity->getFileUri()),
						        'sequence'=>$res[0]['value'],
						        'id'=>$id,
						        'altvalue'=>$node->field_service_image->alt
	    						);
		echo json_encode($service_array);
		exit();*/
	}
}