<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;
use Symfony\Component\HttpFoundation\JsonResponse;
class Homepage_block_two{
  public function page(){
  	global $base_url;  	
  	$success_status = "";
  	$upload_error = "";
  	$error = "";
  	$editor_validate = "";
  if(isset($_SESSION['postid']) ){
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);
      }
    }else{
      $_SESSION['postid'] = rand(10,100);
    }
  	if(!empty($_POST)){
      	if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
	        if($_SESSION['postid'] == $_POST['postid']){
		  		$value    = $_POST['editor1'];
		  		//$detailval    = $_POST['editor2'];
		  		$sequence  = $_POST['sequence'];
		  		$rm_url  = $_POST['rm_url'];
		  		/*if($value == "" || $detailval == ""){*/
		  			if($value == "" || $value == "<p><br></p>"){
		  			$editor_validate = "Please Enter Content";
		  		}else{
					$title    = $_POST['head'];
					if($_POST['hidden_id'] != ""){
						/*echo "up";
						exit;*/
						$node                         = Node::load($_POST['hidden_id']);	
			  			$node->body->value            = $value;
						$node->body->format           = 'full_html';
						//$node->field_homepage_second_data      = $detailval;
						//$node->field_homepage_second_data->format   = 'full_html';
						$node->field_homepage_block2_readmore->value = $rm_url;
						$node->field_homepage_block2_sequence->value = $sequence;
						$node->title                  = $title;
						$node->save();
						$success_status = "Homepage Block Two Updated Successfully";
						
			  		}else{
			  			/*echo "down";
						exit;*/
			  			$node = Node::create([
									'type'  => 'homepage_block_two',
									'title'	=> $title,
									'field_homepage_block2_sequence' => $sequence,
									'field_homepage_block2_readmore' => $rm_url,
									//'field_homepage_second_data' =>['value'=> $detailval,'format'=> 'full_html'],
									'body'	=> ['value'=> $value,'format'=> 'full_html']
									
								]);
						$node->save();
						$success_status = "Homepage Block Two Added Successfully";
			  			
					}
				}	
			}
		}	
		$_SESSION['postid'] = "";	
  	} 
  	if($_SESSION['postid'] == ""){
		$_SESSION['postid'] = rand(10,100);      
	} 
	if($upload_error != ""){
		$error = $upload_error;
	}
	if($editor_validate != ""){
		$error = $editor_validate;
	}
    return array('#theme' => 'homepage_block_two_setting',
    			 '#title' => $success_status,
    			 '#postid'=>$_SESSION['postid'],
    			 '#error'=> $error
				);
  }

  	public function homepage_block_two_url(){  		
		$service_array =[];
	  	$nids          = \Drupal::entityQuery('node')->condition('type','homepage_block_two')->execute();
	  	foreach($nids as $key => $ids){
			$node = \Drupal\node\Entity\Node::load($ids);	
			$res = $node->field_homepage_block2_sequence->getValue();		
			$service_array[] = array(
							        'title'=>$node->getTitle(),
							        'body'=>strip_tags($node->body->value),				
							        'rm_url'=>strip_tags($node->field_homepage_block2_readmore->value),		        
							      //  'detailval'=>strip_tags($node->field_homepage_second_data->value),
							        'id'=>$ids,
							        'sequence'=>$res[0]['value']
	        						);
		}		
		return new JsonResponse([
	      $service_array
	    ]);
  	}

  	public function homepage_block_two_delete(){
     
	    $nodeid = $_POST['id'];
	    $res = array($nodeid=>$nodeid);
		entity_delete_multiple('node', $res);
		die();
  }
  public function homepage_block_two_edit(){      
     
     
      $id   = $_POST['id'];
	  	$node = \Drupal\node\Entity\Node::load($id);
		$res = $node->field_homepage_block2_sequence->getValue();
		$service_array[] = array(
						        'title'=>$node->getTitle(),
						        'body'=>$node->body->value,
						        'id'=>$id,
						       // 'detailval'=>$node->get('field_homepage_second_data')->value,
						        'rm_url'=>$node->get('field_homepage_block2_readmore')->value,
						        'sequence'=>$res[0]['value']
						       
	    						);
		echo json_encode($service_array);
		exit();
  }

  
}