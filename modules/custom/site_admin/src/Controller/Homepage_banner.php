<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\menu_link_content\Entity\MenuLinkContent;
class Homepage_banner{
  public function page(){

  	global $base_url;  	
  	$success_status = "";
  	$upload_error = "";
  	$error = "";
  	$editor_validate = "";
	if(isset($_SESSION['postid']) ){
		if($_SESSION['postid'] == ""){
			$_SESSION['postid'] = rand(10,100);
		}
	}else{
		$_SESSION['postid'] = rand(10,100);
	}
  	if(!empty($_POST)){
      	if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
	        if($_SESSION['postid'] == $_POST['postid']){
		  		$value    = $_POST['editor1'];
		  		$alt      = $_POST['alt'];
		  		if($value == "" || $value == "<p><br></p>"){
		  			$editor_validate = "Please Enter Content";
		  		}else{
					$title    = $_POST['head'];

                 ///

                  if($_FILES["newf"]["tmp_name"] != ""){
						$name        = $_FILES["newf"]["name"];					
						$exts        = explode(".", $name);
						$extension   = $exts[1];
						$allowedExts = array("jpeg", "jpg", "png","PNG","JPEG",'jpg');
						if(in_array($extension, $allowedExts)){
							$target_file =  basename($_FILES["newf"]["name"]);
							move_uploaded_file($_FILES["newf"]["tmp_name"], $target_file);
							//chmod($_FILES["newf"]["name"],0777);
							$data = file_get_contents($base_url."/".$_FILES["newf"]["name"]);
							$file = file_save_data($data, "public://".$_FILES["newf"]["name"], FILE_EXISTS_REPLACE);
						}else{
							$upload_error = "File Type Should Be jpg,png";
						}
					}

			    ////	
					if($_POST['hidden_id'] != ""){

			  			$node                         = Node::load($_POST['hidden_id']);	
			  			$node->body->value            = $value;
						$node->body->format           = 'full_html';
						$node->title                  = $title;
                        
                        if($upload_error == "" ){
							if($_FILES["newf"]["tmp_name"] != ""){
								$field_banner_upload = array(
								    'target_id' => $file->id(),
								    'alt' => $alt,
								    'title' => "My title"
								);
								$node->field_homepage_banner_upload = $field_banner_upload;
							}
								
						}

                      $node->save();
							$success_status = "Homepage Banner Updated Successfully";
						
			  		}else{
			  			/*echo "down";
			  			exit;*/
			  			    if( ($_FILES["newf"]["name"] != "") && ($upload_error == "") ){
			  			$node = Node::create([
									'type'  => 'homepage_banner',
									'title'	=> $title,
									'body'	=> ['value'=> $value,'format'=> 'basic_html'],
									'field_homepage_banner_upload' => [
							    'target_id' => $file->id(),
							    'alt' => $alt,
							    'title' => 'Sample File'
							  ],
									
								]);
						$node->save();
						chmod($_FILES["newf"]["name"],0777);
							unlink($_FILES["newf"]["name"]);
						
						$success_status = "Homepage Banner Added Successfully";
						}else{
							$error = "please upload file";
						}	
			  			
					}
				}	
			}
		}	
		$_SESSION['postid'] = "";	
  	} 
  	if($_SESSION['postid'] == ""){
		$_SESSION['postid'] = rand(10,100);      
	} 
	if($editor_validate != ""){
		$error = $editor_validate;
	}
    return array('#theme' => 'homepage_banner',
    			 '#title' => $success_status,
    			 '#postid'=>$_SESSION['postid'],
    			 '#error'=> $error
				);
  }

  	public function homepagebanner_url(){  		
		$service_array =[];
	  	$nids          = \Drupal::entityQuery('node')->condition('type','homepage_banner')->execute();
	  	foreach($nids as $key => $ids){
			$node = \Drupal\node\Entity\Node::load($ids);			
			$service_array[] = array(
							        'title'=>$node->getTitle(),
							        'body'=>$node->body->value,							        
							        'id'=>$ids,
							        'file'=>file_create_url($node->field_homepage_banner_upload->entity->getFileUri()),
							        'altvalue'=>$node->field_homepage_banner_upload->alt
	        						);
		}		
		return new JsonResponse([
	      $service_array
	    ]);
  	}
}