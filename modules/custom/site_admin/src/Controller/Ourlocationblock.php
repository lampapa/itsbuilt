<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;

class Ourlocationblock{
  public function page(){
  	global $base_url;  	
  	$success_status = "";
  	$upload_error = "";
  	$error = "";
  	$editor_validate = "";
  	if($_SESSION['postid'] == ""){
  		$_SESSION['postid'] = rand(10,100);      
	}
  	if(!empty($_POST)){
      	if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
	        if($_SESSION['postid'] == $_POST['postid']){
		  		$value    = $_POST['editor1'];
		  		$detailval    = $_POST['editor2'];
		  		if($value == "" || $detailval == ""){
		  			$editor_validate = "Please Enter Content,Detail Content";
		  		}else{
					$title    = $_POST['head'];
					if($_POST['hidden_id'] != ""){
						/*echo "up";
						exit;*/
						$node                         = Node::load($_POST['hidden_id']);	
			  			$node->body->value            = $value;
						$node->body->format           = 'full_html';
						$node->field_detail_page      = $detailval;
						$node->field_detail_page->format   = 'full_html';
						$node->title                  = $title;
						$node->save();
						$success_status = "Ourlocation Updated Successfully";
						
			  		}else{
			  			/*echo "down";
						exit;*/
			  			$node = Node::create([
									'type'  => 'location_info',
									'title'	=> $title,
									'field_detail_page' =>['value'=> $detailval,'format'=> 'full_html'],
									'body'	=> ['value'=> $value,'format'=> 'full_html']
									
								]);
						$node->save();
						$success_status = "Ourlocation Added Successfully";
			  			
					}
				}	
			}
		}	
		$_SESSION['postid'] = "";	
  	} 
  	if($_SESSION['postid'] == ""){
		$_SESSION['postid'] = rand(10,100);      
	} 
	if($upload_error != ""){
		$error = $upload_error;
	}
	if($editor_validate != ""){
		$error = $editor_validate;
	}
    return array('#theme' => 'location_setting',
    			 '#title' => $success_status,
    			 '#postid'=>$_SESSION['postid'],
    			 '#error'=> $error
				);
  }

  	public function fullourlocation(){  		
		$service_array =[];
	  	$nids          = \Drupal::entityQuery('node')->condition('type','location_info')->execute();
	  	foreach($nids as $key => $ids){
			$node = \Drupal\node\Entity\Node::load($ids);
			
		    $service_array[] = array(
							        'title'=>$node->getTitle(),
							        'body'=>strip_tags($node->body->value),
							        'id'=>$ids,
							        'detailval'=>strip_tags($node->get('field_detail_page')->value)
	        						);
		}
		echo json_encode($service_array);	
		exit();	
  	}

  	public function ourlocationblockdelete(){
     
	    $nodeid = $_POST['id'];
	    $res = array($nodeid=>$nodeid);
		entity_delete_multiple('node', $res);
		die();
  }
  public function ourlocationblockedit(){ 
    
      
      $id   = $_POST['id'];
	  	$node = \Drupal\node\Entity\Node::load($id);
		/*$res  = $node->field_service_sequence->getValue();		*/
		$service_array[] = array(
						        'title'=>$node->getTitle(),
						        'body'=>$node->body->value,
						        'id'=>$id,
						        'detailval'=>$node->get('field_detail_page')->value
						       
	    						);
		echo json_encode($service_array);
		exit();
  }

  
}