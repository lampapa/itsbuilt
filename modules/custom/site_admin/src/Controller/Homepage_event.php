<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;
use Symfony\Component\HttpFoundation\JsonResponse;
class Homepage_event{
  public function page(){

  	global $base_url;  	
  	$success_status = "";
  	$upload_error = "";
  	$error = "";
  	$editor_validate = "";
  	if(isset($_SESSION['postid']) ){
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);
      }
    }else{
      $_SESSION['postid'] = rand(10,100);
    }
	
  	if(!empty($_POST)){
      	if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
	        if($_SESSION['postid'] == $_POST['postid']){
		  		$value    = $_POST['editor1'];
		  		$detailval    = $_POST['editor2'];
		  		$sequence    = $_POST['sequence'];
		  		if(($value == "" || $value == "<p><br></p>")|| ($detailval == "" || $detailval == "<p><br></p>")){
		  			$editor_validate = "Please Enter Content,Detail Content";
		  		}else{
					$title    = $_POST['head'];
					if($_POST['hidden_id'] != ""){
						/*echo "up";
						exit;*/
						$node                         = Node::load($_POST['hidden_id']);	
			  			$node->body->value            = $value;
						$node->body->format           = 'full_html';
						$node->field_homepage_event_data      = $detailval;
						$node->field_homepage_event_data->format   = 'full_html';
						$node->title                  = $title;
						$node->field_sequence_event->value = $sequence;
						$node->save();
						$success_status = "Events Updated Successfully";
						
			  		}else{
			  			/*echo "down";
						exit;*/
			  			$node = Node::create([
									'type'  => 'homepage_event',
									'title'	=> $title,
									'field_homepage_event_data' =>['value'=> $detailval,'format'=> 'full_html'],
									'field_sequence_event' => $sequence,
									'body'	=> ['value'=> $value,'format'=> 'full_html']
									
								]);
						$node->save();
						$success_status = "Events Added Successfully";
			  			
					}
				}	
			}
		}	
		$_SESSION['postid'] = "";	
  	} 
  	if($_SESSION['postid'] == ""){
		$_SESSION['postid'] = rand(10,100);      
	} 
	if($upload_error != ""){
		$error = $upload_error;
	}
	if($editor_validate != ""){
		$error = $editor_validate;
	}
    return array('#theme' => 'homepage_event_setting',
    			 '#title' => $success_status,
    			 '#postid'=>$_SESSION['postid'],
    			 '#error'=> $error
				);
  }

  	public function homepage_event_url(){  		
		$service_array =[];
	  	$nids          = \Drupal::entityQuery('node')->condition('type','homepage_event')->execute();
	  	foreach($nids as $key => $ids){
			$node = \Drupal\node\Entity\Node::load($ids);
			$res = $node->field_sequence_event->getValue();			
			$service_array[] = array(
							        'title'=>$node->getTitle(),
							        'body'=>strip_tags($node->body->value),						        
							        'detailval'=>strip_tags($node->field_homepage_event_data->value),
							        'id'=>$ids,
							        'sequence'=>$res[0]['value']
	        						);
		}		
		return new JsonResponse([
	      $service_array
	    ]);
  	}

  	public function homepage_event_delete(){
     
	    $nodeid = $_POST['id'];
	    $res = array($nodeid=>$nodeid);
		entity_delete_multiple('node', $res);
		die();
  }
  public function homepage_event_edit(){                
      	$id   = $_POST['id'];
	  	$node = \Drupal\node\Entity\Node::load($id);
		$res = $node->field_sequence_event->getValue();	
		$service_array[] = array(
						        'title'=>$node->getTitle(),
						        'body'=>$node->body->value,
						        'id'=>$id,
						        'detailval'=>$node->get('field_homepage_event_data')->value,
						        'sequence'=>$res[0]['value']
						       
	    						);
		echo json_encode($service_array);
		exit();
  }  
}