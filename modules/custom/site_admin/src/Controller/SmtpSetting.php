<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Database;
use Drupal\Core\Entity\Query\QueryFactory;

class SmtpSetting{
  	public function page(){

	  	global $base_url;  
	  	$success_status = "";
	  	if(isset($_SESSION['postid']) ){
	      if($_SESSION['postid'] == ""){
	        $_SESSION['postid'] = rand(10,100);
	      }
	    }else{
	      $_SESSION['postid'] = rand(10,100);
	    }   
		$error="";$success="";
		
	  	if(!empty($_POST)){
	      	if(!empty($_POST['postid'])){
		        if($_SESSION['postid'] == $_POST['postid']){	
		         $emailid=!empty($_POST['emailid'])?$_POST['emailid']:"";
		         $username=!empty($_POST['username'])?$_POST['username']:"";
		         $password=!empty($_POST['password'])?$_POST['password']:"";
		         $hostid=!empty($_POST['hostid'])?$_POST['hostid']:"";
		         $portno=!empty($_POST['portno'])?$_POST['portno']:"";
		         $protocol=!empty($_POST['protocol'])?$_POST['protocol']:"";
		         $smtp_hidid=!empty($_POST['smtp_hidid'])?$_POST['smtp_hidid']:"";

		         $db = \Drupal::database();

		         if($smtp_hidid==''){
		         	$result = $db->query("INSERT INTO tbl_smtp_setting (emailid,username,password,hostid,portno,protocol) values('$emailid','$username','$password','$hostid','$portno','$protocol')");

			         if($result){
			         	$success="SMTP Settings added succesfully...";
			         }else{
			         	$error="SMTP Settings Failed to add...";
			         }
		         }elseif($smtp_hidid!=''){
		         	$status=!empty($_POST['status'])?$_POST['status']:"N";

		         	if($status=='Y'){
		         		$result = $db->query("SELECT * from tbl_smtp_setting where status='Y' and id!=$smtp_hidid");
		         		while ($row = $result->fetchAssoc()) {
							$smtp_account_array[]=$row;
						 }
						 if(!empty($smtp_account_array)){
						 	$error="Only one account can be active...";
						 }else{
					 		$result = $db->query("UPDATE tbl_smtp_setting SET emailid='$emailid',username='$username',password='$password',hostid='$hostid',portno='$portno',status='$status' where id=$smtp_hidid");

					         if($result){
					         	$success="SMTP Settings updated succesfully...";
					         }else{
					         	$error="SMTP Settings Failed to update...";
					         }
						 }
		         	}else{
			         	$result = $db->query("UPDATE tbl_smtp_setting SET emailid='$emailid',username='$username',password='$password',hostid='$hostid',portno='$portno',status='$status' where id=$smtp_hidid");

				         if($result){
				         	$success="SMTP Settings updated succesfully...";
				         }else{
				         	$error="SMTP Settings Failed to update...";
				         }
				     }
		         }

		         
		         // $result = $db->insert('smtp_setting')->fields($post_data)->execute();

				}	
			}			
			$_SESSION['postid'] = "";	
		}
			
	  	 

	  	if($_SESSION['postid'] == ""){
	  		$_SESSION['postid'] = rand(10,100);      
		}

    	return array('#theme' => 'smtp_setting',
    				 '#title' => $success_status,
    				 '#postid'=>$_SESSION['postid'],
    				 '#success'=>$success,
    				 '#error'=>$error
    				);
  	}

  	public function smtpurl(){  	
		$db = \Drupal::database();
		$service_array=[];
		$result = $db->query("SELECT * from tbl_smtp_setting");
		 while ($row = $result->fetchAssoc()) {
			$service_array[]=$row;
		 }
		echo json_encode($service_array);	
		exit();	
  	}

  	public function deletesmtp(){ 
  		$id = $_POST['id'];	  
	  	$db = \Drupal::database();
		$result = $db->query("DELETE from tbl_smtp_setting where id=$id");
		die();
  	}
  	
	public function smtpedit(){ 
	  	$db = \Drupal::database();
	  	$id   = $_POST['id'];
		$result = $db->query("SELECT * from tbl_smtp_setting where id=$id");
		 while ($row = $result->fetchAssoc()) {
			$service_array[]=$row;
		 }
		echo json_encode($service_array);	
		exit();	
	}
}