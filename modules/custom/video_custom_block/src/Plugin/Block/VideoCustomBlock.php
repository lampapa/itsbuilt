<?php

/**
 * @file
 * Contains \Drupal\video_custom_block\Plugin\Block
 */

namespace Drupal\video_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "video_custom_block",
 *  admin_label = @Translation("Video custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class VideoCustomBlock extends BlockBase{
 
   public function build(){
   	$video_array =[];
    $nids = \Drupal::entityQuery('node')->condition('type','videos')->execute();
    foreach ($nids as $key => $ids) {
      $node = \Drupal\node\Entity\Node::load($ids);   
      $video_array[] = array(
      			$node->getTitle(),$node->body->value,file_create_url($node->field_video_file->entity->getFileUri()),
      			file_create_url($node->field_video_image->entity->getFileUri())
      );
    } 
   // echo "<pre>"; print_r($video_array);
    return [
      '#theme' => 'videotemplate',
      '#test_var' => $this->t('Test Value'),
      '#video' => $video_array
    ];
  }
}

?>