<?php

/**
 * @file
 * Contains \Drupal\keepintouch_custom_block\Plugin\Block
 */

namespace Drupal\keepintouch_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "keepintouch_custom_block",
 *  admin_label = @Translation("Keepintouch custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class KeepintouchCustomBlock extends BlockBase{
 
   public function build(){
      return [
      '#theme' => 'keepintouchtemplate',
      '#test_var' => $this->t('Test Value'),
    ];
  }
}

?>