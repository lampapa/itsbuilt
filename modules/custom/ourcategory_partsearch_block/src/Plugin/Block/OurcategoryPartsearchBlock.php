<?php

/**
 * @file
 * Contains \Drupal\ourcategory_partsearch_block\Plugin\Block
 */

namespace Drupal\ourcategory_partsearch_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "ourcategory_partsearch_block",
 *  admin_label = @Translation("Ourcategory Partsearch Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class OurcategoryPartsearchBlock extends BlockBase{
 
   public function build(){
   	$connection   = \Drupal::database();
    $brand        = [];
    $get_category = $connection->query("SELECT DISTINCT tp.* FROM `taxonomy_term_field_data` tp JOIN commerce_product__field_category_name AS cn ON tp.tid=cn.field_category_name_target_id 
     WHERE vid = 'category' AND STATUS='1'" );
    while($row = $get_category->fetchAssoc()){
        $categories[]=array('category_id'=>$row['tid'],'category_name'=>$row['name']);
    }
    $get_brand = $connection->query("SELECT DISTINCT tp.* FROM `taxonomy_term_field_data` tp JOIN commerce_product__field_brand AS cn ON tp.tid=cn.field_brand_target_id 
     WHERE vid = 'brand' AND STATUS='1'" );
    while($row = $get_brand->fetchAssoc()){
        $brand[]=array('brand_id'=>$row['tid'],'brand_name'=>$row['name']);
    }
      return [
      '#theme' => 'ourcategorypartsearchtemplate',
      '#test_var' => $this->t('Test Value'),
      '#categories' =>$categories,
      '#brand' => $brand,
    ];
  }
}

?>