<?php

/**
 * @file
 * Contains \Drupal\cnc_mycartwish_custom_block\Plugin\Block
 */

namespace Drupal\cncmycartwish_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "cncmycartwish_custom_block",
 *  admin_label = @Translation("Cnc Mycartwish custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class CncmycartwishCustomBlock extends BlockBase{
 
   public function build(){   	 
      return [
      '#theme' => 'cncmycartwishtemplate',
      '#test_var' => $this->t('Test Value'),
    ];
  }
}

?>