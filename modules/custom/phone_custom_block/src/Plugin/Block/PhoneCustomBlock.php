<?php
/**
 * @file
 * Contains \Drupal\phone_custom_block\Plugin\Block
 */
namespace Drupal\phone_custom_block\Plugin\Block;
use Drupal\Core\Block\BlockBase;
/**
 * below section is important
 * 
 * @Block(
 *  id = "phone_custom_block",
 *  admin_label = @Translation("Phone custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class PhoneCustomBlock extends BlockBase{
    public function build(){
    	$connection = \Drupal::database();
	    $query      = $connection->query("SELECT phone from catapult_topbar");
	    $phone='';
	    while ($row = $query->fetchAssoc()){
	    	$phone=$row['phone'];
		}
    	return[
	      '#theme' => 'phonetemplate',
	      '#test_var' => $this->t('Test Value'),
	      '#phone' =>$phone
    	];
  	}
}
?>