<?php

/**
 * @file
 * Contains \Drupal\testimonial_custom_block\Plugin\Block
 */

namespace Drupal\testimonial_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "testimonial_custom_block",
 *  admin_label = @Translation("Testimonial custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class TestimonialCustomBlock extends BlockBase{
 
   public function build(){

   	$testimonial_array =[];
    $nids = \Drupal::entityQuery('node')->condition('type','testimonial')->execute();
    foreach ($nids as $key => $ids) {
	    $node = \Drupal\node\Entity\Node::load($ids);  
	    $testimonial_array[] = array(
	        $node->getTitle(),$node->body->value,file_create_url($node->field_customerimage->entity->getFileUri()),$node->field_location->value);
	} 
    return [
      '#theme' => 'testimonialtemplate',
      '#test_var' => $this->t('Test Value'),
      '#testimonial' =>$testimonial_array,
    ]; 
  }
}

?>