<?php

/**
 * @file
 * Contains \Drupal\footer_custom_block\Plugin\Block
 */

namespace Drupal\footer_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "footer_custom_block",
 *  admin_label = @Translation("Footer custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class FooterCustomBlock extends BlockBase{
 
   public function build(){
      return [
      '#theme' => 'footertemplate',
      '#test_var' => $this->t('Test Value'),
    ];
  }
}

?>