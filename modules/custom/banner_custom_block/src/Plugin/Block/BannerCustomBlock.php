<?php

/**
 * @file
 * Contains \Drupal\my_custom_block\Plugin\Block
 */

namespace Drupal\banner_custom_block\Plugin\Block;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Block\BlockBase;

/**
 * below section is important
 * 
 * @Block(
 *  id = "banner_custom_block",
 *  admin_label = @Translation("Banner custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class BannerCustomBlock extends BlockBase{

  public function build(){
   	$service_array =[];
  	$nids          = \Drupal::entityQuery('node')->condition('type','homepage_topbanner')->execute();
  	foreach($nids as $key => $ids){	  			  		
  		$node = \Drupal\node\Entity\Node::load($ids);				
  		$service_array =$node->body->value;
	  }
      return [
      '#theme' => 'bannertemplate',
      '#test_var' => $this->t('Test Value'),
      '#service_array'=>$service_array
    ];
  }
}
?>