<?php

/**
 * @file
 * Contains \Drupal\contactus_custom_block\Plugin\Block
 */

namespace Drupal\contactus_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "contactus_custom_block",
 *  admin_label = @Translation("Contactus custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class ContactusCustomBlock extends BlockBase{
    public function build(){
	   	$connection = \Drupal::database();
	   	$con_us = [];
	    $query1 = $connection->query("SELECT company_name,address1,address2,zip_code,country,state,city,email,phone,facebook,twitter,youtube,linkedin,googleplus,emaildropline,showmap,googlemap,fax FROM catapult_contact_info");
	    while($row = $query1->fetchAssoc()){
	     $con_us[] = array(
	     	'company_name' => $row['company_name'],
	     	'address1' => $row['address1'],
	     	'address2' => $row['address2'],
	     	'zip_code' => $row['zip_code'],
	     	'country' => $row['country'],
	     	'state' => $row['state'],
	     	'city' => $row['city'],
	     	'email' => $row['email'],
	     	'phone' => $row['phone'],
	     	'facebook' => $row['facebook'],
	     	'twitter' => $row['twitter'],
	     	'youtube' => $row['youtube'],
	     	'linkedin' => $row['linkedin'],
	     	'googleplus' => $row['googleplus'],
	     	'emaildropline' => $row['emaildropline'],
	     	'showmap' => $row['showmap'],
	     	'googlemap' => $row['googlemap'],
	     	'fax' => $row['fax']
	     );
	    }
	  

	    /*echo "<pre>";
	    print_r($con_us);*/
	      return [
	      '#theme' => 'contactustemplate',
	      '#test_var' => $this->t('Test Value'),
	      '#con_us' => $con_us
	    ];
  }
}

?>