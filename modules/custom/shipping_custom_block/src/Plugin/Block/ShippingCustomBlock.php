<?php

/**
 * @file
 * Contains \Drupal\shipping_custom_block\Plugin\Block
 */

namespace Drupal\shipping_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "shipping_custom_block",
 *  admin_label = @Translation("Shipping custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class ShippingCustomBlock extends BlockBase{
 
    public function build(){
		$connection = \Drupal::database();
	    $query      = $connection->query("SELECT shipping from catapult_topbar");
	    $shipping   = '';
	    while ($row = $query->fetchAssoc()){
	    	$shipping=$row['shipping'];
		}
    	return[
    	'#theme' => 'shippingtemplate',
    	'#test_var' => $this->t('Test Value'),
    	'#shipping' =>$shipping
    	];
  	}
}

?>